﻿using O_ScavProtoType.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.DAL.Contracts
{
    public interface IHuntRepo
    {
        //Needs to Get a Users Hunts for admin...
        List<Hunt> GetUserHunts(string UserId);

        //GetHunts for players
        List<Hunt> GetAllHunts();

        Hunt GetHunt(int? id);

        Model.Task GetTask(int? taskId);

        string CreateHunt(Hunt hunt, string userId);

        Model.Task UpdateImageTask(Model.Task updatedTask);

        Model.Task UpdateQuizTask(Model.Task updatedTask);

        QuizAttempt CreateQuizAnswer(QuizAttempt quizAttempt, string userID);

        QuizAttempt GetQuizAttempt(int id);

        ImageAttempt CreateImageAttempt(ImageAttempt imageAttempt, string userID);

        ImageAttempt GetImageAttempt(int id);

        void DeleteTask(int id);

    }
}
