﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.IdentityModel.Claims;
using Microsoft.Owin.Security;
using O_ScavProtoType.Model.UserIdentity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace O_ScavProtoType.DAL.Migrations
{


    public class DbSeederIdentityEF : DropCreateDatabaseAlways<ApplicationDbContext>
    {    // This is useful if you do not want to tear down the database each time you run the application.
         // public class ApplicationDbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
         // This example shows you how to create a new database if the Model changes

        protected override void Seed(ApplicationDbContext context)
        {
            //InitializeIdentityForEF(context);
            base.Seed(context);
        }


        public void AddHuntData(HuntContext context)
        {
            context.Hunts.AddOrUpdate(
              h => h.Id,
              new Hunt() { Name = "Hyde Park" },
              new Hunt() { Name = "Sears Tower" },
              new Hunt() { Name = "South Side" }
            );

            context.POIs.AddOrUpdate(
                p => p.Id,
                new POI() { Name = "57 Street Books", HuntID = 1 },
                new POI() { Name = "Encore", HuntID = 1 },
                new POI() { Name = "Hyde Park Records", HuntID = 1 },

                new POI() { Name = "Sears Tower Window", HuntID = 2 },
                new POI() { Name = "Sears Tower Door", HuntID = 2 },
                new POI() { Name = "Building Next to Sears Tower", HuntID = 2 },

                new POI() { Name = "Field Museum of Natural History", HuntID = 3 },
                new POI() { Name = "Buddy Guy Lenend's", HuntID = 3 },
                new POI() { Name = "Hyde Park Records", HuntID = 3 }
                );

            context.Tasks.AddOrUpdate(
                t => t.PoiID,

                new Task() { Id = 1, TaskText = "The owner of this place is Mr. 57 Street Books", PoiID = 1 },
                new Task() { Id = 2, TaskText = "The task for encore", PoiID = 1 },
                new Task() { Id = 3, TaskText = "Records are sold here", PoiID = 1 },

                new Task() { Id = 4, TaskText = "A Tower is here", PoiID = 2 },
                new Task() { Id = 5, TaskText = "There is a door on a tower here", PoiID = 2 },
                new Task() { Id = 6, TaskText = "There is a building next to a tower here", PoiID = 2 },

                new Task() { Id = 7, TaskText = "There is a lot of natural stuff and history stuff here", PoiID = 3 },
                new Task() { Id = 8, TaskText = "This is the place with your favorite buddy", PoiID = 3 },
                new Task() { Id = 9, TaskText = "This is the place where you can get your favorite records in Hyde Park", PoiID = 3 }
                );
        }

        //Create User=Admin@Admin.com with password=Admin@123456 in the Admin role
        //public static void InitializeIdentityForEF(ApplicationDbContext db)
        //{
        //    var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
        //    var roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
        //    const string name = "admin@example.com";
        //    const string password = "Admin@123456";
        //    const string roleName = "Admin";

        //    //Create Role Admin if it does not exist
        //    var role = roleManager.FindByName(roleName);
        //    if (role == null)
        //    {
        //        role = new AppRole(roleName);
        //        var roleresult = roleManager.Create(role);
        //    }

        //    var user = userManager.FindByName(name);
        //    if (user == null)
        //    {
        //        user = new AppUser { UserName = name, Email = name };
        //        var result = userManager.Create(user, password);
        //        result = userManager.SetLockoutEnabled(user.Id, false);
        //    }

        //    // Add user admin to Role Admin if not already added
        //    var rolesForUser = userManager.GetRoles(user.Id);
        //    if (!rolesForUser.Contains(role.Name))
        //    {
        //        var result = userManager.AddToRole(user.Id, role.Name);
        //    }
        //}


    }
}
