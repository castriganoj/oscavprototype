﻿using O_ScavProtoType.DAL.Contracts;
using O_ScavProtoType.Model;
using O_ScavProtoType.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.DAL
{
    public class HuntTreeRepo : IHuntRepo
    {
        //solely working against the test db right now. Will inject a connectionstring or just different entity context into the repo after tests complete
        private HuntContext db = new HuntContext("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog = TestOscav; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");


        //Reads
        public string CreateHunt(Hunt hunt, string userId)
        {
            //this method should create a hunt with a user should not be working with user hunts app side. 

            var userFromDb = GetUser(hunt.Author.Id);
            hunt.Author = userFromDb;
            db.Hunts.Add(hunt);

            db.SaveChanges();

            return hunt.Id.ToString();
        }

        public List<Hunt> GetUserHunts(string id)
        {
            return db.Hunts.Where(h => h.Author.Id == id).ToList();
        }

        public Hunt GetHunt(int? id)
        {
            return db.Hunts.Find(id);
        }

        public List<Hunt> GetAllHunts()
        {
            return db.Hunts.ToList();
        }


        //Writes
            //Hunts and POIs
        public void DeleteHunt(int huntId)
        {
            var hunt = GetHunt(huntId);
            db.Hunts.Remove(hunt);
            db.SaveChanges();

        }

        //will update hunt and poi 
        public Hunt UpdateHunt(Hunt UpdatedHunt)
        {
            var ParentHuntFromDb = GetHunt(UpdatedHunt.Id);

            if (UpdatedHunt != null)
            {
                // Update parent
                db.Entry(ParentHuntFromDb).CurrentValues.SetValues(UpdatedHunt);

                //Delete children
                //foreach (var existingChildPoi in hunt.Children.ToList())
                //{
                //    if (!ParentHuntFromDb.POIs.Any(c => c.Id == ParentHuntFromDb.Id))
                //        db.POIs.Remove(existingChildPoi);
                //}

                // Update and Insert children
                // Level 1
                foreach (var poiFromUpdatedHunt in UpdatedHunt.POIs)
                {
                    var existingPOI = ParentHuntFromDb.POIs
                        .Where(p => p.Id == poiFromUpdatedHunt.Id)
                        .SingleOrDefault();

                    if (existingPOI != null)
                    {
                        // Update child POI
                        db.Entry(existingPOI).CurrentValues.SetValues(poiFromUpdatedHunt);
                    }

                    else
                    {
                        // Insert child
                        // add POI to Hunt list
                        var newPOI = new POI();
                        newPOI = poiFromUpdatedHunt;
                        ParentHuntFromDb.POIs.Add(newPOI);

                    }
                }
                //save to db after updating entire tree for parent hunt
                db.SaveChanges();
            }

            return UpdatedHunt;
        }


        //Tasks
        public Model.Task UpdateTask(Model.Task task)
        {
            Model.Task taskFromDb = db.Tasks.Find(task.Id);
            db.Entry(taskFromDb).CurrentValues.SetValues(task);

            return task;
        }

        
        //used for updating whole hunt tree
        private void UpdateTasksForPOI(POI poiFromUpdateHunt, POI existingPOI)
        {
            //iterate thru tasks
            foreach (var taskFromUpdatedHunt in poiFromUpdateHunt.Task)
            {
                var existingTask = existingPOI.Task
                        .Where(t => t.Id == taskFromUpdatedHunt.Id)
                        .SingleOrDefault();

                //check if task is old
                //update old diffs
                if (existingTask != null)
                {
                    // Update child
                    db.Entry(existingTask).CurrentValues.SetValues(taskFromUpdatedHunt);
                }

                else
                {
                    // Insert child
                    //add task to poi list
                    var newTask = new Model.Task();
                    newTask = taskFromUpdatedHunt;
                    existingPOI.Task.Add(newTask);
                }
            }
        }

        private ApplicationUser GetUser(string userId)
        {
            return db.Users.Find(userId);
        }

        public void CreateTask(Model.Task task)
        {
            throw new NotImplementedException();
        }

        public List<Hunt> GetUsersHunts(string UserId)
        {
            throw new NotImplementedException();
        }

        public List<Hunt> GetHunts()
        {
            throw new NotImplementedException();
        }

        public Model.Task UpdateImageTask(Model.Task updatedTask)
        {
            throw new NotImplementedException();
        }

        public Model.Task UpdateQuizTask(Model.Task updatedTask)
        {
            throw new NotImplementedException();
        }

        public QuizAttempt CreateQuizAnswer(QuizAttempt quizAttempt, string userID)
        {
            throw new NotImplementedException();
        }

        public QuizAttempt GetQuizAttempt(int id)
        {
            throw new NotImplementedException();
        }

        public ImageAttempt CreateImageAttempt(ImageAttempt imageAttempt, string userID)
        {
            throw new NotImplementedException();
        }

        public ImageAttempt GetImageAttempt(int id)
        {
            throw new NotImplementedException();
        }

        public void DeleteTask(int id)
        {
            throw new NotImplementedException();
        }

        public Model.Task GetTask(int? taskId)
        {
            throw new NotImplementedException();
        }
    }
}
