﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using O_ScavProtoType.DAL;
using O_ScavProtoType.Models;
using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace O_ScavProtoType.DAL
{ 
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    ///     Configure the application user manager used in this application.
    ///     UserManager is defined in ASP.NET Identity and is used by the application.
    /// </summary>
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public class ApplicationUserManager : UserManager<ApplicationUser, string>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser, string> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
        var manager = new ApplicationUserManager(new AppUserStore(context.Get<HuntContext>()));


            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // TODO: add this as a later time
            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            //manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<AppUser>
            //{
            //    MessageFormat = "Your security code is {0}"
            //});
            //manager.SmsService = new SmsService();

            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });

            //why using email service......*********************
            //manager.EmailService = new EmailService();

            // NOTE: changed ASP.NET Identity --> O-Scav User Identity
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("O-Scav User Identity"));
            }

            return manager;
        }
    }
}

///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/// <summary>
///     Custom SignInManager for ASP.NET Identity 2.0 Framework
/// </summary>
///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
{
    ApplicationUserManager _userManager;

    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    ///     Signin manager - contains methods for creating/logging in users
    /// </summary>
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
        : base(userManager, authenticationManager)
    {
        this._userManager = userManager;
    }

    ///***********************************************
    /// <summary>
    ///     Static method that returns new ApplicationSignInManager
    /// </summary>
    /// <param name="options"> IdentityFactoryOptions </param>
    /// <param name="context"> IOwinContext </param>
    /// <returns>
    ///     <see cref="O_ScavProtoType.UI.ApplicationSignInManager"/>
    /// </returns>
    ///***********************************************
    public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
    {
        return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
    }

    ///***********************************************
    /// <summary>
    ///     Generates a user asynchronously
    /// </summary>
    /// <param name="user">
    ///     The AppUser model
    /// </param>
    /// <returns> </returns>
    ///***********************************************
    public override async Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
    {
        var userIdentity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        return userIdentity;
    }

}


///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/// <summary>
///     Custom RoleManager for ASP.NET Identity 2.0 Framework.
/// </summary>
///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
public class ApplicationRoleManager : RoleManager<AppRole>
{
    public ApplicationRoleManager(IRoleStore<AppRole, string> store)
        : base(store)
    { }

    public static ApplicationRoleManager Create(
        IdentityFactoryOptions<ApplicationRoleManager> options,
            IOwinContext context)
    {
        return new ApplicationRoleManager(
            new AppRoleStore(context.Get<HuntContext>()));
    }
}

//#region Custom Identity Types
/////---------------------------------------------------------------------------------------------------
///// <summary>
/////     For extending Identity Framework.
/////     This is taken from various tutorials online, most of it coming from John Atten's
/////         tutorial on extending IdentityFramework
///// </summary>
///// <see cref="http://johnatten.com/2014/07/20/asp-net-identity-2-0-extensible-template-projects/"/>
/////     You will not likely need to customize there, but it is necessary/easier to create our own
/////     project-specific implementations, so here they are:
/////---------------------------------------------------------------------------------------------------
//public class ApplicationUserLogin : IdentityUserLogin<string> { }
//public class ApplicationUserClaim : IdentityUserClaim<string> { }
//public class ApplicationUserRole : IdentityUserRole<string> { }

//#endregion



// Most likely won't need to customize these either, but they were needed because we implemented
// custom versions of all the other types:
public class AppUserStore
        : UserStore<ApplicationUser, AppRole, string, AppUserLogin, AppUserRole, AppUserClaim>,

        IQueryableUserStore<ApplicationUser, string>,
                IDisposable
{
    public AppUserStore()
        : this(new IdentityDbContext())
    {
        base.DisposeContext = true;
    }

    public AppUserStore(DbContext context)
        : base(context)
    {
    }
}


public class AppRoleStore
    : RoleStore<AppRole, string, AppUserRole>, IQueryableRoleStore<AppRole, string>,
        IRoleStore<AppRole, string>,
            IDisposable
{
    public AppRoleStore()
        : base(new IdentityDbContext())
    {
        base.DisposeContext = true;
    }

    public AppRoleStore(DbContext context)
        : base(context)
    {
    }
}


