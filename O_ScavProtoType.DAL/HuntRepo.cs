﻿using O_ScavProtoType.DAL.Contracts;
using O_ScavProtoType.Model;
using O_ScavProtoType.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.DAL
{
    public class HuntRepo : IHuntRepo
    {
        private HuntContext db = new HuntContext();

        //Reads
        public string CreateHunt(Hunt hunt, string userId)
        {
            var userFromDb = GetUser(userId);
            hunt.Author = userFromDb;
            db.Hunts.Add(hunt);

            db.SaveChanges();

            return hunt.Id.ToString();
        }

        public List<Hunt> GetUserHunts(string id)
        {
            return db.Hunts.Where(h => h.Author.Id == id).ToList();
        }

        public Hunt GetHunt(int? id)
        {
            return db.Hunts.Find(id);
        }

        public List<Hunt> GetAllHunts()
        {
            return db.Hunts.ToList();
        }

        //Need Test for GetTask()
        public Model.Task GetTask(int? taskId)
        {
            return db.Tasks.Find(taskId);
        }


        //Writes
        public void DeleteHunt(int huntId)
        {
            var hunt = GetHunt(huntId);
            db.Hunts.Remove(hunt);
            db.SaveChanges();

        }

        public Hunt UpdateHunt(Hunt UpdatedHunt)
        {
            var ParentHuntFromDb = GetHunt(UpdatedHunt.Id);

            if (UpdatedHunt != null)
            {
                // Update parent
                db.Entry(ParentHuntFromDb).CurrentValues.SetValues(UpdatedHunt);

                //Delete children
                foreach (var poiChildFromDb in ParentHuntFromDb.POIs.ToList())
                {
                    if (!UpdatedHunt.POIs.Any(p => p.Id == poiChildFromDb.Id))
                        db.POIs.Remove(poiChildFromDb);
                }

                // Update and Insert children
                // Level 1
                foreach (var poiFromUpdatedHunt in UpdatedHunt.POIs)
                {
                    var existingPOI = ParentHuntFromDb.POIs
                        .Where(p => p.Id == poiFromUpdatedHunt.Id)
                        .SingleOrDefault();

                    if (existingPOI != null)
                    {
                        // Update child POI
                        db.Entry(existingPOI).CurrentValues.SetValues(poiFromUpdatedHunt);
                    }

                    else
                    {
                        // Insert child
                        // add POI to Hunt list
                        var newPOI = new POI();
                        newPOI = poiFromUpdatedHunt;
                        ParentHuntFromDb.POIs.Add(newPOI);

                    }
                }
                //save to db after updating entire tree for parent hunt
                db.SaveChanges();
            }

            return UpdatedHunt;
        }

        public Model.Task CreateTask(Model.Task task)
        {
            db.Tasks.Add(task);
            db.SaveChanges();
            return task;
        }

        public Model.Task UpdateImageTask(Model.Task updatedTask)
        {
            Model.Task taskFromDb = db.Tasks.Find(updatedTask.Id);
            db.Entry(taskFromDb).State = EntityState.Modified;

            File updatedFile = updatedTask.TaskType.Image.Files.Single();

            taskFromDb.TaskType.Image.Files[0].FileName = updatedFile.FileName;
            taskFromDb.TaskType.Image.Files[0].Content = updatedFile.Content;
            taskFromDb.TaskType.Image.Files[0].ContentType = updatedFile.ContentType;
            taskFromDb.TaskText = updatedTask.TaskText;


            db.SaveChanges();

            return taskFromDb;
        }

        public Model.Task UpdateQuizTask(Model.Task updatedTask)
        {
            Model.Task taskFromDb = db.Tasks.Find(updatedTask.Id);
            db.Entry(taskFromDb).State = EntityState.Modified;

            taskFromDb.TaskType.Quiz.QuizAnswers = updatedTask.TaskType.Quiz.QuizAnswers;
            db.SaveChanges();

            return taskFromDb;
        }

        public void DeleteTask(int id)
        {
            Model.Task taskToRemove = db.Tasks.Find(id);
            //need to set up cascade delete in annotations so this image
            //or quiz logic isnt needed. 
          

            if (taskToRemove.TaskType.TaskTypeNum == TaskTypeNum.Image)
            {
                db.Files.Remove(taskToRemove.TaskType.Image.Files.Single());
                db.CorrectImage.Remove(taskToRemove.TaskType.Image);
            }
            else
            {
                db.QuizAnswers.Remove(taskToRemove.TaskType.Quiz.QuizAnswers);
                db.TaskTypes.Remove(taskToRemove.TaskType);
                db.Tasks.Remove(taskToRemove);
                db.SaveChanges();
            }

        }


        public QuizAttempt CreateQuizAnswer(QuizAttempt quizAttempt, string userID)
        {
            
            var user = GetUser(userID);
            quizAttempt.User = user;

            db.QuizAttempts.Add(quizAttempt);
            try
            {
                db.SaveChanges();
            }
            catch(Exception ex)
            {
               string message =  ex.Message;
            }

            return quizAttempt;
        }

        public QuizAttempt GetQuizAttempt(int id)
        {
           return db.QuizAttempts.Find(id);
        }

        public ImageAttempt CreateImageAttempt(ImageAttempt imageAttempt, string userId)
        {
            var user = GetUser(userId);

            imageAttempt.AppUser = user;
            imageAttempt.CorrectImage = getCorrectImage(imageAttempt.CorrectImage.Id);

            db.ImageAttempt.Add(imageAttempt);
            
            db.SaveChanges();

            return imageAttempt;
        }

        public ImageAttempt GetImageAttempt(int id)
        {
            return db.ImageAttempt.Find(id);
        }



        private void UpdateTasksForPOI(POI poiFromUpdateHunt, POI existingPOI)
        {
            //iterate thru tasks
            foreach (var taskFromUpdatedHunt in poiFromUpdateHunt.Task)
            {
                var existingTask = existingPOI.Task
                        .Where(t => t.Id == taskFromUpdatedHunt.Id)
                        .SingleOrDefault();

                //check if task is old
                //update old diffs
                if (existingTask != null)
                {
                    // Update child
                    db.Entry(existingTask).CurrentValues.SetValues(taskFromUpdatedHunt);
                }

                else
                {
                    // Insert child
                    //add task to poi list
                    var newTask = new Model.Task();
                    newTask = taskFromUpdatedHunt;
                    existingPOI.Task.Add(newTask);
                }
            }
        }

        private ApplicationUser GetUser(string userId)
        {
            return db.Users.Find(userId);
        }

        private ImageTask getCorrectImage(int id)
        {
           return db.CorrectImage.Find(id);
        }

    }
}
