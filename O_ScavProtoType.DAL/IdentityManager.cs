﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O_ScavProtoType.Models;
using O_ScavProtoType.DAL;

namespace O_ScavProtoType.DAL
{
    ///--------------------------------------------------------------
    /// <summary>
    ///     Manages Users & roles, acting as a medium b/w the data & entity layers
    /// </summary>
    /// <remarks>
    ///     Multiple roles (ie, Administrator, Builder, Player)
    ///     One-to-Many association b/w Users and Roles (Builder --> userId = 1111)
    /// </remarks>
    /// ///--------------------------------------------------------------
    public class IdentityManager
    {
        ApplicationRoleManager _roleManager = new ApplicationRoleManager(
            new AppRoleStore(new HuntContext()));

        ApplicationUserManager _userManager = new ApplicationUserManager(
            new AppUserStore(new HuntContext()));

        HuntContext _db = new HuntContext();

        public object CreateUser(ApplicationUser appUser)
        {
            return _userManager.CreateIdentity(appUser, DefaultAuthenticationTypes.ApplicationCookie);
        }

        #region Properties

        ///***********************************************
        /// <summary>
        ///     Summary not available.
        /// </summary>
        /// <param name="name">
        ///     Checks if role name exists
        /// </param>
        ///***********************************************
        public bool RoleExists(string name)
        {
            return _roleManager.RoleExists(name);
        }

        #endregion


        #region Users

        #region Finding User

        ///***********************************************
        /// <summary>
        ///     Finds user by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        ///***********************************************
        public ApplicationUser FindByName(string name)
        {
            return _userManager.FindByName(name);
        }

        #endregion



        #region User Creation

        ///***********************************************
        /// <summary>
        ///     Creates roles
        /// </summary>
        /// <param name="name">
        ///     Name of role.
        /// </param>
        /// <param name="description">
        ///     Roles description.
        /// </param>
        ///***********************************************
        public bool CreateRole(string name, string id, string description)
        {
            // Swap ApplicationRole for IdentityRole:
            var idResult = _roleManager.Create(new AppRole(name, id, description));
            return idResult.Succeeded;
        }

        ///***********************************************
        /// <summary>
        ///     Creates user.
        /// </summary>
        /// <param name="user">
        ///     Description not available not available.
        /// </param>
        /// <param name="password">
        ///     Password of user
        /// </param>
        ///***********************************************
        public bool CreateUser(ApplicationUser user, string password)
        {
            var idResult = _userManager.Create(user, password);
            return idResult.Succeeded;
        }

        #endregion

        #region User Roles

        ///***********************************************
        /// <summary>
        ///     Adds a role to a particular user
        /// </summary>
        /// <param name="userId">
        ///     userId of user
        /// </param>
        /// <param name="roleName">
        ///     Name of role to add to user
        /// </param>
        ///***********************************************
        public bool AddUserToRole(string userId, string roleName)
        {
            var idResult = _userManager.AddToRole(userId, roleName);
            return idResult.Succeeded;
        }


        ///***********************************************
        /// <summary>
        ///     Clears.removes roles
        /// </summary>
        /// <param name="userName">
        ///     The user whose role(s) are removed
        /// </param>
        ///***********************************************
        public void ClearUserRoles(string userName)
        {
            var user = _userManager.FindByName(userName);
            var currentRoles = new List<AppUserRole>();

            currentRoles.AddRange(user.Roles);
            foreach (var role in currentRoles)
            {
                _userManager.RemoveFromRole(userName, role.Role.Name);
            }
        }

        #endregion




        #endregion
    }
}
