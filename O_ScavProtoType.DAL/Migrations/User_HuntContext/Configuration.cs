namespace O_ScavProtoType.DAL.Migrations.User_HuntContext
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using O_ScavProtoType.Model.HuntModels;
    using O_ScavProtoType.Model;

    internal sealed class Configuration : DbMigrationsConfiguration<O_ScavProtoType.DAL.HuntContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\User_HuntContext";
        }

        protected override void Seed(O_ScavProtoType.DAL.HuntContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            AddHuntItems(context);
        }

        public static void AddHuntItems(HuntContext context)
        {
            context.Hunts.AddOrUpdate(
              h => h.Id,
              new Hunt() { Name = "Hyde Park" },
              new Hunt() { Name = "Sears Tower" },
              new Hunt() { Name = "South Side" }
            );

            context.POIs.AddOrUpdate(
                p => p.Id,
                new POI() { Name = "57 Street Books", HuntID = 1 },
                new POI() { Name = "Encore", HuntID = 1 },
                new POI() { Name = "Hyde Park Records", HuntID = 1 },

                new POI() { Name = "Sears Tower Window", HuntID = 2 },
                new POI() { Name = "Sears Tower Door", HuntID = 2 },
                new POI() { Name = "Building Next to Sears Tower", HuntID = 2 },

                new POI() { Name = "Field Museum of Natural History", HuntID = 3 },
                new POI() { Name = "Buddy Guy Lenend's", HuntID = 3 },
                new POI() { Name = "Hyde Park Records", HuntID = 3 }
                );

            context.Tasks.AddOrUpdate(
                t => t.PoiID,

                new Task() { Id = 1, TaskText = "The owner of this place is Mr. 57 Street Books", PoiID = 1 },
                new Task() { Id = 2, TaskText = "The task for encore", PoiID = 1 },
                new Task() { Id = 3, TaskText = "Records are sold here", PoiID = 1 },

                new Task() { Id = 4, TaskText = "A Tower is here", PoiID = 2 },
                new Task() { Id = 5, TaskText = "There is a door on a tower here", PoiID = 2 },
                new Task() { Id = 6, TaskText = "There is a building next to a tower here", PoiID = 2 },

                new Task() { Id = 7, TaskText = "There is a lot of natural stuff and history stuff here", PoiID = 3 },
                new Task() { Id = 8, TaskText = "This is the place with your favorite buddy", PoiID = 3 },
                new Task() { Id = 9, TaskText = "This is the place where you can get your favorite records in Hyde Park", PoiID = 3 }
                );
        }
    }
}
