namespace O_ScavProtoType.DAL.Migrations.User_HuntContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserToHuntRelationships : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        FileId = c.Int(nullable: false, identity: true),
                        FileName = c.String(maxLength: 255),
                        ContentType = c.String(maxLength: 100),
                        Content = c.Binary(),
                        FileType = c.Int(nullable: false),
                        poiId = c.Int(nullable: false),
                        Image_Id = c.Int(),
                    })
                .PrimaryKey(t => t.FileId)
                .ForeignKey("dbo.POIs", t => t.poiId, cascadeDelete: true)
                .ForeignKey("dbo.Images", t => t.Image_Id)
                .Index(t => t.poiId)
                .Index(t => t.Image_Id);
            
            CreateTable(
                "dbo.POIs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Image = c.Binary(),
                        HuntID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Hunts", t => t.HuntID, cascadeDelete: true)
                .Index(t => t.HuntID);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskText = c.String(),
                        PoiID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.POIs", t => t.PoiID, cascadeDelete: true)
                .Index(t => t.PoiID);
            
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        Image = c.Binary(),
                        TaskID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tasks", t => t.TaskID, cascadeDelete: true)
                .Index(t => t.TaskID);
            
            CreateTable(
                "dbo.TaskTypes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        TaskTypeNum = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tasks", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TaskTypes", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Quizs",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        PotentialAnswersId = c.Int(nullable: false),
                        AnswerAttempt = c.String(),
                        QuizAnswers_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuizAnswers", t => t.QuizAnswers_Id)
                .ForeignKey("dbo.TaskTypes", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.QuizAnswers_Id);
            
            CreateTable(
                "dbo.QuizAnswers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AnswerA = c.String(),
                        AnswerB = c.String(),
                        AnswerC = c.String(),
                        AnswerD = c.String(),
                        CorrectAnsert = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Hunts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        PoiID = c.Int(nullable: false),
                        Latitude = c.Single(nullable: false),
                        Longitutde = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserHuntContents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HuntID = c.Int(nullable: false),
                        UserHunt_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserHunts", t => t.UserHunt_Id)
                .Index(t => t.UserHunt_Id);
            
            CreateTable(
                "dbo.UserTasks",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        AppUserID = c.String(),
                        TaskID = c.Int(nullable: false),
                        UserHuntContent_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserHuntContents", t => t.UserHuntContent_Id)
                .Index(t => t.UserHuntContent_Id);
            
            CreateTable(
                "dbo.UserHunts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AppUserID = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserHuntContents", "UserHunt_Id", "dbo.UserHunts");
            DropForeignKey("dbo.UserTasks", "UserHuntContent_Id", "dbo.UserHuntContents");
            DropForeignKey("dbo.POIs", "HuntID", "dbo.Hunts");
            DropForeignKey("dbo.Tasks", "PoiID", "dbo.POIs");
            DropForeignKey("dbo.TaskTypes", "Id", "dbo.Tasks");
            DropForeignKey("dbo.Quizs", "Id", "dbo.TaskTypes");
            DropForeignKey("dbo.Quizs", "QuizAnswers_Id", "dbo.QuizAnswers");
            DropForeignKey("dbo.Images", "Id", "dbo.TaskTypes");
            DropForeignKey("dbo.Files", "Image_Id", "dbo.Images");
            DropForeignKey("dbo.Answers", "TaskID", "dbo.Tasks");
            DropForeignKey("dbo.Files", "poiId", "dbo.POIs");
            DropIndex("dbo.UserTasks", new[] { "UserHuntContent_Id" });
            DropIndex("dbo.UserHuntContents", new[] { "UserHunt_Id" });
            DropIndex("dbo.Quizs", new[] { "QuizAnswers_Id" });
            DropIndex("dbo.Quizs", new[] { "Id" });
            DropIndex("dbo.Images", new[] { "Id" });
            DropIndex("dbo.TaskTypes", new[] { "Id" });
            DropIndex("dbo.Answers", new[] { "TaskID" });
            DropIndex("dbo.Tasks", new[] { "PoiID" });
            DropIndex("dbo.POIs", new[] { "HuntID" });
            DropIndex("dbo.Files", new[] { "Image_Id" });
            DropIndex("dbo.Files", new[] { "poiId" });
            DropTable("dbo.UserHunts");
            DropTable("dbo.UserTasks");
            DropTable("dbo.UserHuntContents");
            DropTable("dbo.Locations");
            DropTable("dbo.Hunts");
            DropTable("dbo.QuizAnswers");
            DropTable("dbo.Quizs");
            DropTable("dbo.Images");
            DropTable("dbo.TaskTypes");
            DropTable("dbo.Answers");
            DropTable("dbo.Tasks");
            DropTable("dbo.POIs");
            DropTable("dbo.Files");
        }
    }
}
