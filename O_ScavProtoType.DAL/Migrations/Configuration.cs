namespace O_ScavProtoType.DAL.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<O_ScavProtoType.DAL.HuntContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations";
        }

        // Current seed is a temporary building block to a more well 
        // structured seeding architecture that will use a static config 
        // file to configure default values for app membership.
        protected override void Seed(HuntContext context)
        {

            //System.Diagnostics.Debugger.Launch();

            if (!context.Roles.Any(r => r.Name == "Administrator"))
            {
                var idManager = new IdentityManager();

                idManager.CreateRole("Administrator", "Administrator", "1");
                idManager.CreateRole("Builder", "Builder", "2");
                idManager.CreateRole("Player", "Player", "3");
            }

            if (!context.Users.Any(r => r.UserName == "Administratordoe"))
            {
                ApplicationUser AdministratorUser = new ApplicationUser();
                AdministratorUser.Email = "AdministratorDoe@gmail.com";
                AdministratorUser.UserName = "Administratordoe";

                var idManager = new IdentityManager();

                idManager.CreateUser(AdministratorUser, "123ap1T7!");

                string userID = idManager.FindByName(AdministratorUser.UserName).Id;
                idManager.AddUserToRole(userID, "Administrator");
            }

            if (!context.Users.Any(u => u.UserName == "builderdoe"))
            {
                ApplicationUser builderUser = new ApplicationUser();
                builderUser.Email = "builderDoe@gmail.com";
                builderUser.UserName = "builderdoe";

                var idManager = new IdentityManager();

                idManager.CreateUser(builderUser, "123ap1T7!");

                string userID = idManager.FindByName(builderUser.UserName).Id;
                idManager.AddUserToRole(userID, "Builder");
            }

            if (!context.Users.Any(u => u.UserName == "playerdoe"))
            {
                ApplicationUser playerUser = new ApplicationUser();
                playerUser.Email = "playerDoe@gmail.com";
                playerUser.UserName = "playerdoe";

                var idManager = new IdentityManager();

                idManager.CreateUser(playerUser, "123ap1T7!");

                string userID = idManager.FindByName(playerUser.UserName).Id;
                idManager.AddUserToRole(userID, "Player");
            }
        }
    }
}
