﻿using Microsoft.AspNet.Identity.EntityFramework;
using O_ScavProtoType.Model;
using O_ScavProtoType.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics;
using System.Collections.Generic;

namespace O_ScavProtoType.DAL
{
    public class HuntContext : IdentityDbContext<ApplicationUser, AppRole, string, AppUserLogin, AppUserRole, AppUserClaim>
    {
        public HuntContext(): base("O-scav")
        {
            Database.Log = sql => Debug.Write(sql);
           
            //if a connection string is passed in Create a new database. 
            //currently being used for DAL integration tests. 

 }

        public HuntContext(string dataBaseName) : base(dataBaseName)
        {
            Database.CreateIfNotExists();
            Database.Log = sql => Debug.Write(sql);

        }

        public DbSet<Hunt> Hunts { get; set; }
        public DbSet<POI> POIs { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<TaskType> TaskTypes { get; set; }
        public DbSet<ImageTask> CorrectImage { get; set; }
        public DbSet<QuizTask> Quizzes { get; set; }
        public DbSet<QuizAttempt> QuizAttempts { get; set; }
        public DbSet<ImageAttempt> ImageAttempt { get; set; }
        public DbSet<QuizAnswers> QuizAnswers { get; set; }
        public static HuntContext Create()
        {
            return new HuntContext();
        }

        public List<Hunt> GetHunts()
        {
            throw new NotImplementedException();
        }
    }
}
