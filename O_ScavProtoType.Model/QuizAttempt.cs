﻿using O_ScavProtoType.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.Model
{
    public class QuizAttempt
    {
        public int Id { get; set; }
        public string Answer {get;set;}
        public bool Correct { get; set; }


        public QuizTask Quiz { get; set; }
        public ApplicationUser User { get; set; }

    }
}
