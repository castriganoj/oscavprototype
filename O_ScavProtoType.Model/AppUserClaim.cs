using Microsoft.AspNet.Identity.EntityFramework;

namespace O_ScavProtoType.Models
{

    public partial class AppUserClaim : IdentityUserClaim<string>
    {
        /// <summary>
        ///     Information about user login.
        /// </summary>
        public AppUserClaim()
        {
        }

        /////////////////////////////////////////////
        /// <summary>
        ///     Description of social media service.
        /// </summary>
        /////////////////////////////////////////////
        public virtual string Description { get; set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     Description of social media service.
        /// </summary>
        /////////////////////////////////////////////
        public virtual string SecretKeyAPI { get; set; }

        public virtual string Token { get; set; }

    }
}