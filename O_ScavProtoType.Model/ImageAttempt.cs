﻿using O_ScavProtoType.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.Model

{
     public class ImageAttempt
    {
        public int Id { get; set; }
        public bool Correct { get; set; }

        public File Image { get; set; }

        public ImageTask CorrectImage { get; set; }
        public ApplicationUser AppUser { get; set; }
    }
}
