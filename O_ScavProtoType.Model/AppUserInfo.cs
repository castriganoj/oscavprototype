﻿using System;
using System.Collections.Generic;

namespace O_ScavProtoType.Models
{

    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    ///     User extra info contained in this table
    /// </summary>
    /// <remarks>
    ///     Contains triggers <see cref="ITriggerable"/>
    /// </remarks>
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public class AppUserInfo
    {

        /////////////////////////////////////////////
        /// <summary>
        ///     PRIMARY KEY - in this <see cref="ApplicationUser"/> extender table
        /// </summary>
        /////////////////////////////////////////////
        public string Id { get; private set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     FOREGIN KEY - First name of user
        /// </summary>
        /////////////////////////////////////////////
        public int AppUserID { get; set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     Organization - only for Users (possibly Administrators)
        /// </summary>
        /////////////////////////////////////////////
        public string Organization { get; set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     User birth date
        /// </summary>
        /////////////////////////////////////////////
        public DateTime? BirthDate { get; set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     First name of user
        /// </summary>
        /////////////////////////////////////////////
        public string FirstName { get; set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     Last name of user
        /// </summary>
        /////////////////////////////////////////////
        public string LastName { get; set; }

        ///////////////////////////////////////////
        /// <summary>
        ///     One-to-Many relationships with UserHunts
        /// </summary>
        ///////////////////////////////////////////

    }
}