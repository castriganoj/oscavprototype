﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace O_ScavProtoType.Models
{
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    /// Roles:
    ///     1. Builder
    ///     2. Player
    ///     3. Administrator
    /// </summary>
    /// <remarks>
    ///     ApplicationRole Entity type for Asp.Net.IdentityModel
    /// </remarks>
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public class AppRole : IdentityRole<string, AppUserRole>
    {
        #region Constructors

        public AppRole() : base()
        { }

        public AppRole(string name)
            :this()
        {
            base.Name = name;
        }

        public AppRole(string name, string description, string id)
            : this(name)
        {
            this.Description = description;
            this.Name = name;
            this.Id = id;
        }

        #endregion Constructors

        #region Properties

        /////////////////////////////////////////////
        /// <summary>
        ///     App role description
        /// </summary>
        /////////////////////////////////////////////
        public virtual string Description { get; set; }
        public string Name { get; set; }

        #endregion Properties

    }
}