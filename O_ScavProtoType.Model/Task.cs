﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace O_ScavProtoType.Model
{
    public class Task
    {
        public int Id { get; set; }
        public string TaskText { get; set; }


        public int PoiID { get; set; }

        public virtual TaskType TaskType { get; set; }
        public virtual List<Answer> Answers { get; set; }

    }
}
