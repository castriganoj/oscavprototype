﻿using System.Collections.Generic;

/// <summary>
///     Geolocation of user during a hunt.
/// </summary>
namespace O_ScavProtoType.Model
{
    public class Location
    {
        public string Id { get; set; }

        public int PoiID { get; set; }

        public float Latitude { get; set; }

        public float Longitutde { get; set; }



    }
}
