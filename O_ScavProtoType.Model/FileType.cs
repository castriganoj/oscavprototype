﻿namespace O_ScavProtoType.Model
{
    public enum FileType
    {
        PoiImage = 1,
        CorrectImageAnswer = 2,
        AttemptImageAnswer = 3
    }
}