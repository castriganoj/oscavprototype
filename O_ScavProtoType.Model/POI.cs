﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.Model
{
    public class POI
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int HuntID { get; set; }

        
        public virtual List<Task> Task { get; set; }
        public virtual List<File> Files { get; set;}

        public POI()
        {
            Task = new List<Task>();
        }
    }
}
