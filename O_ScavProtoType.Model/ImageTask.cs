﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace O_ScavProtoType.Model
{
    public class ImageTask
    {
        [Key, ForeignKey("TaskType")]
        public int Id { get; set; }

        [Required]
        public virtual TaskType TaskType { get; set; }
        [Required]
        public virtual List<File> Files { get; set; }
    }
}