using Microsoft.AspNet.Identity.EntityFramework;

namespace O_ScavProtoType.Models
{
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    ///     Custom model representing roles for each individual user
    /// </summary>
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public partial class AppUserRole : IdentityUserRole<string>
    {
        public virtual AppRole Role { get; set; }

        public virtual ApplicationUser User { get; set; }


    }
}