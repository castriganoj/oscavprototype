﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using O_ScavProtoType.Model;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

///`````````````````````````````````````````````````````
/// <summary>
///     Identity model contains user classes and methods for authorization.
/// </summary>
/// <remarks>
///     Main user login class/table.
/// </remarks>
///`````````````````````````````````````````````````````
namespace O_ScavProtoType.Models
{
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ///     ApplicationUser was moved to O_ScavPrototType.Model project since it is used in the DbContext
    ///       You can add profile data for the user by adding more properties to your AppUser class,
    ///        please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    /// <summary>
    ///     User Role as string
    ///     Must be expressed in terms of our custom Role and other types:
    /// </summary>
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public class ApplicationUser: IdentityUser<string, AppUserLogin, AppUserRole, AppUserClaim>
    {

        public ApplicationUser()
        {
            Id = Guid.NewGuid().ToString();
            LastLogin = DateTime.Now;
        }

        #region Extended Info


        /////////////////////////////////////////////////
        /// <summary>
        ///     Other user info goes here. Can personalize
        /// </summary>
        /////////////////////////////////////////////////
        public virtual AppUserInfo AppUserInfo { get; set; }

        /////////////////////////////////////////////////
        /// <summary>
        ///     The time user spends while logged in
        /// </summary>
        /////////////////////////////////////////////////
        public long SessionDuration { get; set; }

        /////////////////////////////////////////////////
        /// <summary>
        ///     What was the last date of user login.
        /// </summary>
        /////////////////////////////////////////////////
        public DateTime LastLogin { get; set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     Summary: Navigation property for user roles
        /// </summary>
        /////////////////////////////////////////////
        public virtual List<AppUserRole> AppUserRoles { get; set; }

        public virtual List<Hunt> Hunts { get; set; }


        #endregion

        #region Methods

        //public Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager)
        //{
        //    return Task.FromResult(GenerateUserIdentity(manager));
        //}

        //public ClaimsIdentity GenerateUserIdentity(ApplicationUserManager manager)
        //{
        //    // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
        //    var userIdentity = manager.CreateIdentity(this, DefaultAuthenticationTypes.ApplicationCookie);

        //    // Add custom user claims here


        //    return userIdentity;
        //}

        #endregion
    }
}