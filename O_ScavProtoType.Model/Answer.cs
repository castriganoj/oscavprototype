﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.Model
{
    public class Answer
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public byte[] Image { get; set; }

        public int TaskID { get; set; }
    }
}
