﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.Model
{
    public class QuizAnswers
    {
        //Were limited to 4 answers with this implementation
        //I'm not comfortable dynamically adding columns to a table because it
        //may become susceptible to normal form violations. We would need to switch
        //to a shema that adds answers as new rows but this would make querying difficult.
        //There are likely other more complex options to be explored.

        public int Id { get; set; }
        public string AnswerA {get; set; }
        public string AnswerB {get; set; }
        public string AnswerC {get; set; }
        public string AnswerD {get; set; }
        public string CorrectAnsert { get; private set; }

        //May need to think about how to restrict setting this property to a specific object
        public void setCorrectAnswer(string answer)
        {
            this.CorrectAnsert = answer;
        }

    }
}
