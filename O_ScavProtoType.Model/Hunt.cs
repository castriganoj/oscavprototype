﻿using O_ScavProtoType.Models;
using System.Collections.Generic;
using System.ComponentModel;

namespace O_ScavProtoType.Model
{
    public class Hunt
    {
        public int Id { get; set; }

        [DisplayName("Hunt Name")]
        public string Name { get; set; }


        public virtual List<POI> POIs { get; set; }

        public virtual ApplicationUser Author { get; set; }



        //public override bool Equals(object obj)
        //{
        //        return this.Id == (obj as Hunt).Id;
        //}
    }


}
