﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace O_ScavProtoType.Model
{
    public class QuizTask
    {
        [Key,ForeignKey("TaskType")]
        public int Id { get; set; }
        public int PotentialAnswersId { get; set; }

        public string AnswerAttempt { get; set; }

        [Required]
        public virtual QuizAnswers QuizAnswers { get; set; }

        [Required]
        public virtual TaskType TaskType {get;set;}

    }
}