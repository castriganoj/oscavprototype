﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.Model
{
    public class TaskType
    {
        [Key, ForeignKey("Task")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public TaskTypeNum TaskTypeNum { get; set; }

        public virtual QuizTask Quiz { get; set; }
        public virtual ImageTask Image { get;  set; }

        [Required]
        public virtual Task Task { get; set; }

    }

}
