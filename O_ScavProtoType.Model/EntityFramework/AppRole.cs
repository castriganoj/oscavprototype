﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/// <summary>
/// Roles:
///  1. Builder
///  2. Player
///  3. Admin
/// </summary>
/// <remarks>
///     ApplicationRole Entity type for Asp.Net.IdentityModel
/// </remarks>
///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
namespace O_ScavProtoType.Model.IdentityModel
{

    public class AppRole : IdentityRole
    {
        public AppRole() : base() { }

        public AppRole(string name, string description)
            : base(name)
        {
            Name = name;
            Description = description;
        }

        /////////////////////////////////////////////
        /// <summary>
        ///     App role description
        /// </summary>
        /////////////////////////////////////////////
        [Required]
        public virtual string Description { get; set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     Summary not available
        /// </summary>
        /////////////////////////////////////////////
        [Required]
        [StringLength(128)]
        public string Discriminator { get; set; }
    }

}
