﻿using EntityFramework.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.Model.IdentityModel
{
    public abstract class AppUserTracker : ITriggerable
    {
        public DateTime InsertedDate { get; protected set; }

        public DateTime UpdatedDate { get; protected set; }

        public Boolean IsDeleted { get; protected set; }

        static AppUserTracker()
        {
            Triggers<AppUserTracker>.Inserting += entry => entry.Entity.InsertedDate = entry.Entity.UpdatedDate = DateTime.Now;
            Triggers<AppUserTracker>.Updating += entry => entry.Entity.UpdatedDate = DateTime.Now;
            Triggers<AppUserTracker>.Deleting += entry =>
            {
                entry.Entity.IsDeleted = true;
                entry.Cancel(); // Cancels the deletion, but will persist changes with the same effects as EntityState.Modified
            };
        }
    }


    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    ///     User extra info contained in this table
    /// </summary>
    /// <remarks>
    ///     Contains triggers <see cref="ITriggerable"/>
    /// </remarks>
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public class AppUserInfo : ITriggerable
    {
        #region Constructors

        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        ///     Triggers contained in constructor
        /// </summary>
        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public AppUserInfo()
        {
            this.Triggers().Inserting += entry => { entry.Entity.InsertedCreatedDate = DateTime.Now; };

            this.Triggers().Updating += entry => { entry.Entity.Updated = DateTime.Now; };
        }

        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        ///     Full name set by user
        /// </summary>
        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public AppUserInfo(string fullName)
        {
            this.FullName = fullName;
            this.Triggers().Updating += entry => { entry.Entity.Updated = DateTime.Now; };
        }

        #endregion

        #region Properties

        /////////////////////////////////////////////
        /// <summary>
        ///     Primary key in table as a large integer (64-bit unsigned)
        /// </summary>
        /////////////////////////////////////////////
        public UInt64 Id { get; private set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     First name of user
        /// </summary>
        /////////////////////////////////////////////
        public string FirstName { get; set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     Last name of user
        /// </summary>
        /////////////////////////////////////////////
        public string LastName { get; set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     Full name of user - can be inserted.
        /// </summary>
        /////////////////////////////////////////////
        public string FullName
        {
            get
            {
                return !string.IsNullOrEmpty(this.FullName) ? this.FullName : null;
            }

            protected set
            {
                if (string.IsNullOrEmpty(value))
                {
                    this.Triggers().Inserting += entry =>
                    {
                        entry.Entity.FullName = entry.Entity.FirstName.PadRight(1) +
                                                        entry.Entity.LastName;
                    };
                }
                else
                {
                    FullName = value;
                }
            }
        }

        /////////////////////////////////////////////
        /// <summary>
        ///     Organization user represents
        /// </summary>
        /////////////////////////////////////////////
        public string Organization { get; set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     Date user was created (signed up)
        /// </summary>
        /////////////////////////////////////////////
        public DateTime InsertedCreatedDate { get; private set; }

        /////////////////////////////////////////////
        /// <summary>
        ///     Gets updated when user changes any data in table
        /// </summary>
        /////////////////////////////////////////////
        public DateTime Updated { get; private set; }

        ////////////////////////////////////////////
        /// <summary>
        ///     Email confirmed
        /// </summary>
        /////////////////////////////////////////////
        public bool EmailConfirmed { get; set; }

        #endregion

    }
}
