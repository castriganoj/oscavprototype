namespace O_ScavProtoType.Model.IdentityModel
{
    using System.ComponentModel.DataAnnotations;
    using Microsoft.AspNet.Identity.EntityFramework;

    public partial class AppUserRole : IdentityUserRole
    {
        public int Id { get; set; }

        public string AppUserID { get; set; }

        public string AppRoleID { get; set; }

        /// <summary>
        ///     The number of roles assigned to users.
        /// </summary>
        public string RoleCount { get; set; }

        [Required]
        [StringLength(128)]
        /////////////////////////////////////////
        /// <summary>
        ///     Description of individual user roles
        /// </summary>
        /////////////////////////////////////////
        public string Discriminator { get; set; }

        /// tests
        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    this.OnModelCreating(modelBuilder);
        //}
    }
}
