﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.Services.Common
{
    public class HttpUtils
    {

        #region HTML decoder

        public static string DecodeHTML()
        {
            string txt = "";



            return txt;
        }

        #endregion

        public List<StreamWriter> Subscribers { get; private set; }

        public Socket _locker { get; set; }

        public HttpResponseMessage WriteLogHttpException(Exception ex)
        {
            var response = new HttpResponseMessage
            {
                Content = new PushStreamContent(async (respStream, content, context) =>
                {
                    var subscriber = new StreamWriter(respStream)
                    {
                        AutoFlush = true
                    };
                    lock (_locker)
                    {
                        Subscribers.Add(subscriber);
                    }
                    await subscriber.WriteLineAsync("data: \n");
                }, "text/event-stream")
            };

            return response;
        }

    }
}
