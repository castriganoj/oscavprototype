﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.Services.Common
{

    public static class TimeManager
    {


        #region Timer Method

        public static void StartDispatchTimer(long duration, long startTime = 0)
        {

        }


        public static void UpdateTimer()
        {

        }



        /// <summary>
        ///     Customize this timer for whatever application you may need it for.
        /// </summary>
        public class CustomTimer
        {


            #region Fields

            public readonly static DateTime CurrentDateTime = DateTime.Now;

            public readonly long StartTime = 0;

            #endregion

            #region Constructors

            ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            /// <summary>
            ///     Base class for timer - extend this in other constructors for whatever purpose
            /// </summary>
            ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            public CustomTimer() { }


            public CustomTimer(long duration)
            {

            }


            #endregion
        }

        #endregion
    }


}
