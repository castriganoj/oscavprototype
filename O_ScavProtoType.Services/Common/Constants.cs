﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace O_ScavProtoType.Services.Common
{
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    ///     Constants such as messages, emails, etc.
    /// </summary>
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public class Constants
    {
        #region Credentials

        ///|||||||||||||||||||||||||||||||||||||||||||||||
        /// <summary>
        ///     Administrator UserName for database
        /// </summary>
        ///|||||||||||||||||||||||||||||||||||||||||||||||
        public static string O_ScavDbAdministrator = ConfigurationManager.AppSettings["dbAdministratorUserName"];

        ///|||||||||||||||||||||||||||||||||||||||||||||||
        /// <summary>
        ///     Administrator UserName for database
        /// </summary>
        ///|||||||||||||||||||||||||||||||||||||||||||||||
        public static string O_ScavDbAdministratorPwd = ConfigurationManager.AppSettings["dbAdministratorPassword1"];

        #region Emails

        #region Zoho Account

        public static string O_ScavZohoEmail = ConfigurationManager.AppSettings["zohoEmail"];

        public static readonly string O_ScavZohoEmailPwd = ConfigurationManager.AppSettings["zohoPwd"];

        public static readonly int O_ScavZohoPort = int.Parse(ConfigurationManager.AppSettings["zohoPort"]);

        public static readonly string O_ScavZohoSmtpHost = ConfigurationManager.AppSettings["zohoSmtpHost"];

        public static readonly bool O_ScavZohoEnableSSL = bool.Parse(ConfigurationManager.AppSettings["zohoEnableSSL"]);

        #endregion

        #region Gmail Account

        public static readonly string O_ScavGmailAddress = ConfigurationManager.AppSettings["gmailAddress"];

        public static string O_ScavGmailSmtp = ConfigurationManager.AppSettings["mailSmtpHost"];

        public static bool O_ScavGmailSsl = bool.Parse(ConfigurationManager.AppSettings["enableSSL"]);

        public static int O_ScavGmailTimeout = int.Parse(ConfigurationManager.AppSettings["mailTimeout"]);

        public static int O_ScavGmailPort = int.Parse(ConfigurationManager.AppSettings["mailPort"]);

        public static readonly string O_ScavGmailPwd = ConfigurationManager.AppSettings["gmailPwd"];


        #endregion

        /// <summary>
        ///     Email used for logging in as Administrator. Use for Seeding DB - explore@o-scav.com
        /// </summary>
        public static string O_ScavMainEmail = ConfigurationManager.AppSettings["AdministratorEmail"];

        public static readonly string SendGridEmail = ConfigurationManager.AppSettings["sendgridEmail"];

        #endregion

        #region Dev Auth Login

        public static string O_ScavAuthEmail = ConfigurationManager.AppSettings["zohoEmail"];

        /// <summary>
        ///     Administrator password for Identity Framework
        /// </summary>
        public static string O_ScavAuthPwd = ConfigurationManager.AppSettings["testPassword1"];

        public static readonly string O_ScavAuthOrg = ConfigurationManager.AppSettings["companyName"];

        public static readonly string O_ScavAuthAdministratorUsername = ConfigurationManager.AppSettings["AdministratorUserName"];

        /// <summary>
        ///     Dummy email/username prefix - add a number to end
        /// </summary>
        public static readonly string DummyUserName_Prefix = "dummyuser";

        public static readonly string DummyUserEmail_Postfix = "@oscavtestr.com";

        public static readonly string DummyUserPwd = "testPasswd411";

        public static readonly string DummyUserOrganization = "Dummy INC";

        #endregion

        #region SendGrid

        public static string SendGrid_DisplayName = ConfigurationManager.AppSettings["companyDisplayName"];

        public static string SendGrid_EmailAddress = ConfigurationManager.AppSettings["sendgridEmail"];

        public static string SendGrid_Username = ConfigurationManager.AppSettings["sendgridUsername"];

        public static string SendGrid_Pwd = ConfigurationManager.AppSettings["sendgridPwd"];

        #endregion

        #endregion


        #region Message Strings

        public static string RegistrationMessage = App_LocalResources.Resources.EmailMessage_HtmlInterest;

        public static string Email_MessageInterest = App_LocalResources.Resources.EmailMessage_HtmlInterest;

        public static string Email_Message2 { get { return ""; } }

        #endregion


        #region Roles (Identity Framework)

        public static readonly string Administrator = "Administrator";

        public static readonly string Builder = "Builder";

        public static readonly string Player = "Player";

        #region Role Descriptions

        public static readonly string DefaultDescription_Administrator = "Administrator privileges.";

        public static readonly string DefaultDescription_Player = "User that participates in hunts.";

        public static readonly string DefaultDescription_Builder = "User that builds hunts.";

        public static Dictionary<string, string> RoleDict = new Dictionary<string, string>() { {Administrator, DefaultDescription_Administrator }
                                                                , {Builder, DefaultDescription_Builder}
                                                                , { Player, DefaultDescription_Player } };

        #endregion

        #endregion


        #region API Keys

        ///`````````````````````````````````````````````````````
        /// <summary>
        ///     Keys, client IDs & secrets for third-party APIs authorization.
        /// </summary>
        ///`````````````````````````````````````````````````````
        public static class ApiKeys
        {
            #region Google API(s)

            public static string GooglePlusClientId = ConfigurationManager.AppSettings["gpClientId"];

            public static string GooglePlusSecret = ConfigurationManager.AppSettings["gpClientSecret"];

            #endregion

            #region Twitter API

            public static string TwitterConsumerKey = ConfigurationManager.AppSettings["twtrConsumerKey"];

            public static string TwitterConsumerSecret = ConfigurationManager.AppSettings["twtrConsumerSecret"];

            #endregion

            public static string SendGridAPIKey = ConfigurationManager.AppSettings["sendgridAPIKey"];

        }

        #endregion
    }
}