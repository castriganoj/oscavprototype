﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O_ScavProtoType.Services.Common
{

    public static class StringUtils
    {

        #region Fields

        #region Common String Separators

        public static readonly string DoubleQuoteString = "\"";

        public static readonly string SingleQuoteString = "'";

        public static readonly string ColonString = ":";

        public static readonly string SpaceString = " ";

        public static readonly string CommaString = ",";

        public static readonly string NewLineString = Environment.NewLine;

        public static readonly string TabString = "\t";


        #endregion

        #endregion

        ///***********************************************************
        /// <summary>
        ///     Converts an array of string values into a single string around a separator.
        ///     Also
        /// </summary>
        /// <param name="seperator"> The value that separates strings in array </param>
        /// <param name="arr"> An array </param>
        /// <param name="ends"> What goes at the start & end </param>
        /// <param name="format"> String value to format around </param>
        /// <returns> Single string value </returns>
        ///***********************************************************
        public static string JoinArray(string seperator, string[] arr, string ends, string format = "{0}")
        {
            // turn into a List, then use
            var list = new List<string>(arr);
            string joined = JoinList(seperator, list, ends, format: (format ?? "{0}"));
            LogReturnedString(joined);

            return joined;
        }

        ///***********************************************************
        /// <summary>
        ///     Joins a <see cref="List{T}"/> of strings with a separator
        ///      after joining the individual items in the list together
        ///     Allows adding a non-default format value
        ///     - ie, format = "'{0}'" // adds a single quote (for JSON)
        /// </summary>
        /// <param name="separator"> Separator character </param>
        /// <param name="list"> List of string values </param>
        /// <param name="format"> Can be empty or must have value of {0} </param>
        /// <returns> Formatted single string value </returns>
        ///***********************************************************
        public static string JoinList(string separator, List<string> list, string ends, string format = "{0}")
        {
            string joinedString = ends + string.Join(separator, list.Select(x => string.Format(format, x))) + ends;
            LogReturnedString(joinedString);

            return joinedString;
        }



        #region Logging

        public static void LogReturnedString(string returnVal)
        {
            Console.WriteLine(returnVal);
        }

        #endregion
    }
}
