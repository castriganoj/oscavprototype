﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace O_ScavProtoType.Services.Common
{
    ///`````````````````````````````````````````````````````
    /// <summary>
    ///     For writing log file and/or showing exceptions in the console.
    /// </summary>
    ///`````````````````````````````````````````````````````
    public static class WriteLog
    {
        //public static bool _isInnerException = true;

        //public static int _exceptionMessageCount = 4;

        //public static void LogWriteToFile(string exception)
        //{
        //    string execPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        //    try
        //    {

        //    }
        //    catch(Exception ex)
        //    {
        //        Console.Write(ex.ToString());
        //    }
        //}

        ///***********************************************
        /// <summary>
        ///     Writes exception to console
        /// </summary>
        /// <param name="ex">
        ///     Exception
        /// </param>
        ///***********************************************
        public static void LogWriteConsole(Exception ex)
        {
            try
            {
                if (ex.Data.Count > 0)
                {
                    Console.WriteLine("  Extra details:");
                    foreach (DictionaryEntry de in ex.Data)
                    {
                        Console.WriteLine("    Key: {0,-20}      Value: {1}", "'" + de.Key.ToString() + "'", de.Value);
                    }
                }

            }
            catch
            {
                Console.WriteLine("Exception occurred!" + ex.TargetSite);
                throw new NotImplementedException();
            };

        }


        ///***********************************************
        /// <summary>
        ///     Writes exception to file
        /// </summary>
        /// <param name="ex">
        ///     Exception
        /// </param>
        ///***********************************************
        public static void WriteLogException(Exception ex)
        {
            var path = @"c:\temp\log.txt";

            if (!File.Exists(path))
            {
                if (ex.Data.Count > 0)
                {
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        try
                        {
                            sw.WriteLine("Exception Details: " + StringUtils.NewLineString);

                            var listDetails = GetExceptionDetails(ex);
                            foreach (string s in listDetails)
                            {
                                sw.WriteLine(s);
                            }

                        }
                        catch
                        {
                            Console.WriteLine("Exception occurred at: " + ex.TargetSite);
                            throw new NotImplementedException();
                        };
                    }

                }

            }
        }

        ///***********************************************
        /// <summary>
        ///     Gets the exception basics
        /// </summary>
        /// <param name="ex"> Exception to get info from </param>
        /// <returns></returns>
        ///***********************************************
        public static string GetExceptionBasics(Exception ex)
        {
            string exData = "{0}{1}{2}{3}";
            string[] msg = { string.Format(StringUtils.NewLineString + "Message ---{0}", ex.Message),
                            string.Format(StringUtils.NewLineString + "Source ---\n{0}", ex.Source),
                            string.Format(StringUtils.NewLineString + "HelpLink ---\n{0}", ex.HelpLink),
                            string.Format(StringUtils.NewLineString + "StackTrace ---\n{0}", ex.StackTrace),
                            string.Format(StringUtils.NewLineString + "TargetSite ---\n{0}", ex.TargetSite)
                           };
            string.Format(exData, msg);


            return exData;
        }

        ///***********************************************
        /// <summary>
        ///     Gets exception details as a list
        ///     If StreamWriter exists, then will write to it.
        /// </summary>
        /// <param name="ex"> Exception </param>
        /// <param name="sw"> StreamWriter object </param>
        /// <returns> A List of strings containing details </returns>
        ///***********************************************
        public static List<string> GetExceptionDetails(Exception ex, StreamWriter sw = null)
        {
            var listDetails = new List<string>();
            var detailFormat = "      Key: {0,-20}   =>  Value: {1} " + StringUtils.NewLineString;

            if (ex.Data.Count > 0)
            {
                foreach (DictionaryEntry de in ex.Data)
                {
                    string s = string.Format(detailFormat, "'" + de.Key.ToString() + "'", de.Value);

                    if (sw != null)
                    {
                        sw.WriteLine(s);
                    }
                    else
                    {
                        listDetails.Add(s);
                    }
                }
            }
            else
            {
                listDetails.Add("No details!");
                if (sw != null)
                {
                    sw.WriteLine("No details!");
                }
            }

            return listDetails;
        }

    }
}
