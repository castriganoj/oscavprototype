﻿using Microsoft.AspNet.Identity;
using SendGrid;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;

namespace O_ScavProtoType.Services.IdentityServices
{
    public class MessageServices
    {
        #region Email Services
        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        ///    Email service uses SendGrid API
        /// </summary>
        /// <remarks>
        ///     Method based on blog post <see cref=""/>
        /// </remarks>
        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public class EmailService : IIdentityMessageService
    {
        #region Fields

        private readonly string OScavEmailPwd = Services.Common.Constants.O_ScavGmailPwd;

        private readonly string OScavClient = Services.Common.Constants.O_ScavGmailSmtp;

        private readonly int OScavGmailPort = Services.Common.Constants.O_ScavGmailPort;

        private readonly bool IsOScavSSLEnabled = Services.Common.Constants.O_ScavGmailSsl;

        private readonly string OScavEmailAddress = Services.Common.Constants.SendGrid_EmailAddress;

        private readonly string OScavSendGridUsername = Services.Common.Constants.SendGrid_Username;

        private readonly string OScavSendGridPwd = Services.Common.Constants.SendGrid_Pwd;

        private readonly string OScavSendGridDisplay = Services.Common.Constants.SendGrid_DisplayName;

        #endregion

        #region Properties

        ////////////////////////////////////////
        /// <summary>
        ///     Set this before sending email
        /// </summary>
        ////////////////////////////////////////
        public string EmailFrom { get; set; }

        ////////////////////////////////////////
        /// <summary>
        ///     This should be new users email.
        /// </summary>
        ////////////////////////////////////////
        public string EmailTo { get; set; }

        ////////////////////////////////////////
        /// <summary>
        ///     This is the user's full name
        /// </summary>
        ////////////////////////////////////////
        public string DisplayName { get; set; }

        #endregion

        #region Constructors

        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        ///     Email service empty constructor
        /// </summary>
        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public EmailService() : base() { }

        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        ///     Constructor for email service
        /// </summary>
        /// <param name="emailFrom">
        ///     This is O-Scav email
        /// </param>
        /// <param name="emailTo">
        ///     Users email
        /// </param>
        /// <param name="displayName">
        ///     Display name
        /// </param>
        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public EmailService(string emailFrom, string emailTo, string displayName)
        {
            this.EmailTo = emailTo;
            this.EmailFrom = emailFrom;
            this.DisplayName = displayName;
        }

        #endregion


        #region Methods

        ///*****************************************************************
        /// <summary>
        ///     Sends a message asynchronously - main message method.
        /// </summary>
        /// <param name="message">
        ///     Message to be sent.
        /// </param>
        /// <returns>
        ///     Threading Task or null.
        /// </returns>
        ///*****************************************************************
        public Task SendAsync(IdentityMessage message)
        {
            var credentialUserName = Services.Common.Constants.O_ScavGmailAddress;
            var sentFrom = credentialUserName;

            try
            {
                return SendMail_Route1(message, credentialUserName, sentFrom);
            }
            catch (Exception ex)
            {
                Services.Common.WriteLog.LogWriteConsole(ex);
                return null;
            }
            finally
            {
                ConfigSendGridasync(message);
            }
        }

        ///******************************************************
        /// <summary>
        ///     First route - via gmail account.
        /// </summary>
        /// <param name="message">
        ///     IdentityMessage for user.
        /// </param>
        /// <param name="credentialUserName">
        ///     O-Scav credentials
        /// </param>
        /// <param name="sentFrom">
        ///     O-Scav gmail
        /// </param>
        /// <returns>
        ///
        /// </returns>
        ///******************************************************
        public Task SendMail_Route1(IdentityMessage message, string credentialUserName, string sentFrom)
        {
            // Configure the client (from System.Net.Mail)
            var client = new SmtpClient(OScavClient);

            client.Port = OScavGmailPort;
            client.EnableSsl = IsOScavSSLEnabled;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;

            // Create the O-Scav email credentials:
            NetworkCredential credentials = new NetworkCredential(credentialUserName, OScavSendGridPwd);
            client.Credentials = credentials;

            // Create the message:
            var mail = new MailMessage(sentFrom, message.Destination);

            mail.Subject = message.Subject;
            mail.Body = message.Body;

            // Send email for confirming registration
            return client.SendMailAsync(mail);
        }


        ///******************************************************
        /// <summary>
        ///     Sends a message to user that registered.
        /// </summary>
        /// <param name="message">
        ///     IdentityMessage class containing message with registration linkn
        /// </param>
        /// <returns>
        ///     Description not available
        /// </returns>
        ///******************************************************
        protected Task ConfigSendGridasync(IdentityMessage message)
        {
            var myMessage = new SendGridMessage();

            myMessage.AddTo(message.Destination);
            myMessage.From = new MailAddress(OScavEmailAddress, OScavSendGridDisplay);
            myMessage.Subject = message.Subject;
            myMessage.Text = message.Body;
            myMessage.Html = message.Body;

            var credentials = new NetworkCredential(OScavEmailAddress, OScavEmailPwd);

            // Create a Web transport for sending email.
            var transportWeb = new Web(credentials);

            // Send the email.
            Task returnedMailObj = transportWeb.DeliverAsync(myMessage);

            return returnedMailObj;
        }

        ///******************************************************
        /// <summary>
        ///     Sends email to user normal way
        /// </summary>
        /// <param name="message">
        ///     MailMessage object to be sent
        /// </param>
        ///******************************************************
        public void SendMail(MailMessage message)
        {
            string text = string.Format("Please click on this link to {0}: {1}", message.Subject, message.Body);
            string html = "Please confirm your account by clicking this link: <a href=\"" + message.Body + "\">link</a><br/>";

            html += HttpUtility.HtmlEncode(@"Or click on the copy the following link on the browser:" + message.Body);

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(EmailFrom ?? OScavEmailAddress, DisplayName);
            msg.To.Add(new MailAddress(EmailTo));
            msg.Subject = message.Subject;

            // Message views
            msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
            msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

            SmtpClient smtpClient = new SmtpClient( Common.Constants.O_ScavGmailSmtp, Convert.ToInt32(587));
            NetworkCredential credentials = new NetworkCredential(Common.Constants.O_ScavGmailAddress, Common.Constants.O_ScavGmailAddress);
            smtpClient.Credentials = credentials;
            smtpClient.EnableSsl = true;
            smtpClient.Send(msg);
        }

        #endregion
    }

    #endregion


        #region SMS Services

        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        ///     SMS services
        ///     TODO: add Twilio API here
        /// </summary>
        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public class SmsService : IIdentityMessageService
        {
            public Task SendAsync(IdentityMessage message)
            {
                // Plug in your SMS service here to send a text message.
                return Task.FromResult(0);
            }
        }

        #endregion
    }


}
