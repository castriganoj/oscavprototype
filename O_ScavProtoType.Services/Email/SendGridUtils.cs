﻿using Newtonsoft.Json; // You can generate your JSON string yourelf or with another library if you prefer
using O_ScavProtoType.Services.Common;
using SendGrid;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;

namespace O_ScavProtoType.Services.Email
{
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    ///     Most code adapted from SendGrid's GitHub
    ///     - using C# Web API v3 library on GitHub
    ///     NOTES
    ///     - SendGrid API Key placed in User Environmental Variable
    /// </summary>
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static class SendGridUtils
    {
        #region Keys/Tags

        private static string _apiKey = Environment.GetEnvironmentVariable("SENDGRID_APIKEY", EnvironmentVariableTarget.User);

        private static dynamic sg = new SendGridAPIClient(_apiKey);

        private static string _register_Template_ID = ConfigurationManager.AppSettings["Registration"];

        #region Template Keys

        private static string TemplateId = "'template_id'";
        private static string VersionId = "'version_id'";
        public static string BuilderRegisterTemplate = "builderRegister";

        public static string PlayerRegisterTemplate = "playerRegister";

        #endregion

        #region Template Substitution Tags


        public static string Register_Name = "[name]";

        public static string Register_RegisterURL = "[signupURL]";

        public static string Register_LoginURL = "[loginURL]";

        public static string Register_LoginEmail = "[loginEmail]";

        public static string[] Register_SubTags = { Register_Name, Register_RegisterURL, Register_LoginURL,
                                                        Register_LoginURL, Register_LoginEmail};

        public static string User_NewPwd = "[newPassword]";

        #endregion



        #endregion

        #region API Keys

        ///**************************************************************
        /// <summary>
        ///     Retrieve all API Keys belonging to the authenticated user
        ///     GET /api_keys
        /// </summary>
        ///**************************************************************
        public static async Task<IAsyncResult> RetrieveAPIKeys()
        {
            string queryParams = @"{
                                  'limit': 1
                                }";
            dynamic response = await sg.client.api_keys.get(queryParams: queryParams);
            LogResponseSG(response);

            return response;
        }

        ///**************************************************************
        /// <summary>
        ///     Update the name & scopes of an API Key
        ///     PUT /api_keys/{api_key_id}
        ///     - Example: 'scopes':[ 'user.profile.update', ...]
        /// </summary>
        /// <param name="apiName"></param>
        /// <param name="scopeArr"></param>
        /// <returns> Task as asynchronous RESTful response </returns>
        ///**************************************************************
        public static async Task<IAsyncResult> UpdateAPIKey(string apiName, List<string> scopeArr)
        {
            // Concat each string with "'" at beginning & end, along with "," b/w each
            string formatList = StringUtils.JoinList(",", scopeArr, StringUtils.SingleQuoteString);

            // Add name & scopes
            string data = string.Format(@"{
                              'name': {0},
                              'scopes': [
                                {1}
                              ]
                            }", apiName, formatList);
            Object json = JsonConvert.DeserializeObject<Object>(data);
            data = json.ToString();
            var api_key_id = "test_url_param";
            dynamic response = await sg.client.api_keys._(api_key_id).put(requestBody: data);

            LogResponseSG(response);

            return response;
        }

        #endregion


        #region Create Template

        ///**************************************************************
        /// <summary>
        ///     Create a transactional template.
        ///     POST /templates
        /// </summary>
        /// <param name="api"></param>
        /// <returns>Task as asynchronous RESTful response </returns>
        ///**************************************************************
        public static async Task<IAsyncResult> CreateSendGridTemplate(string data)
        {
            Object json = JsonConvert.DeserializeObject<Object>(data);
            data = json.ToString();
            dynamic response = await sg.client.templates.post(requestBody: data);
            LogResponseSG(response);

            return response;
        }

        ///**************************************************************
        /// <summary>
        ///     Create a new transactional template version.
        ///     POST /templates/{template_id}/versions
        /// </summary>
        /// <param name="data"> Data to add to template </param>
        /// <param name="template_id"> The template ID </param>
        /// <returns>Task as asynchronous RESTful response </returns>
        ///**************************************************************
        public static async Task<IAsyncResult> CreateNewTemplateVersion(string data, string template_id)
        {
            Object json = JsonConvert.DeserializeObject<Object>(data);
            data = json.ToString();
            dynamic response = await sg.client.templates._(template_id).versions.post(requestBody: data);
            LogResponseSG(response);


            return response;
        }

        #endregion


        #region Template Substitutions

        /// <summary>
        ///     Gets a SendGrid message template. Uses dictionary whose keys that are equal to
        ///      the tagged values within SendGrid messages & get replaced by those keys values.
        /// </summary>
        /// <param name="mailFrom"> From email, ie. explore@oscav.com </param>
        /// <param name="mailTo"> Usually a users email </param>
        /// <param name="templateId"> ID of template </param>
        /// <param name="replaceTokens"> Dictionary of tokesn to replace </param>
        /// <returns></returns>
        public static SendGridMessage GetSendGridMessage(string mailFrom, string mailTo, string templateId,
            Dictionary<string, string> replaceTokens)
        {
            var mail = new SendGridMessage { From = new MailAddress(mailFrom)};


            mail.EnableTemplateEngine(templateId);

            foreach(var key in replaceTokens.Keys)
            {
                mail.AddSubstitution(key, new List<string> { replaceTokens[key] });
            }

            return mail;
        }

        #endregion


        #region Retrieve Templates

        ///**************************************************************
        /// <summary>
        ///     Retrieve all transactional templates.
        ///     GET /templates
        /// </summary>
        /// <returns>Task as asynchronous RESTful response </returns>
        ///**************************************************************
        public static async Task<IAsyncResult> RetrieveAllTemplates()
        {
            dynamic response = await sg.client.templates.get();
            LogResponseSG(response);

            return response;
        }

        ///**************************************************************
        /// <summary>
        ///     Retrieve a specific transactional template version.
        ///     GET /templates/{template_id}/versions/{version_id}
        /// </summary>
        /// <param name="template_id"> Template ID </param>
        /// <param name="version_id"> Version ID </param>
        /// <returns> Task as asynchronous RESTful response </returns>
        ///**************************************************************
        public static async Task<IAsyncResult> RetrieveTemplateVersion(string template_id, string version_id)
        {
            dynamic response = await sg.client.templates._(template_id).versions._(version_id).get();
            LogResponseSG(response);

            return response;
        }


        ///**************************************************************
        /// <summary>
        ///     Retrieve a single transactional template.
        ///     GET /templates/{template_id}
        /// </summary>
        /// <param name="template_id">  Template ID </param>
        /// <returns> Task as asynchronous RESTful response </returns>
        ///**************************************************************
        public static async Task<IAsyncResult> RetrieveSingleTemplate(string template_id)
        {
            dynamic response = await sg.client.templates._(template_id).get();
            LogResponseSG(response);


            return response;
        }

        #endregion


        #region Delete Template

        /// <summary>
        ///     Delete a template.
        ///     DELETE /templates/{template_id}
        /// </summary>
        /// <param name="template_id"> Template ID </param>
        /// <returns></returns>
        public static async Task<IAsyncResult> DeleteSingleTemplate(string template_id)
        {
            dynamic response = await sg.client.templates._(template_id).delete();
            LogResponseSG(response);


            return response;
        }


        ////////////////////////////////////////////////////////
        // Delete a transactional template version.
        // DELETE /templates/{template_id}/versions/{version_id}
        public static async Task<IAsyncResult> DeleteTemplateVersion(string template_id, string version_id)
        {
            dynamic response = await sg.client.templates._(template_id).versions._(version_id).delete();
            LogResponseSG(response);


            return response;
        }

        #endregion


        #region Edit Template

        ///**************************************************************
        /// <summary>
        ///     Edit a transactional template.
        ///     PATCH /templates/{template_id}
        /// </summary>
        /// <param name="dataAsString"> JSON data as a string </param>
        /// <param name="templateId">  Template ID </param>
        /// <returns></returns>
        ///**************************************************************
        public static async Task<IAsyncResult> EditTemplateWithID(string dataAsString, string templateId)
        {
            string data = dataAsString;
            Object json = JsonConvert.DeserializeObject<Object>(data);
            data = json.ToString();
            var template_id = templateId;
            dynamic response = await sg.client.templates._(template_id).patch(requestBody: data);
            LogResponseSG(response);

            return response;
        }


        ///**************************************************************
        /// <summary>
        ///     Edit a transactional template version.
        ///     PATCH /templates/{template_id}/versions/{version_id}
        /// </summary>
        /// <param name="template_id"> Template Id </param>
        /// <param name="version_id"> Version Id </param>
        /// <param name="data"> json string, ie, string s = "@{'name':'example'}</param>
        /// <returns></returns>
        ///**************************************************************
        public static async Task<IAsyncResult> EditTemplateVersion(string template_id, string version_id, string data)
        {
            Object json = JsonConvert.DeserializeObject<Object>(data);
            data = json.ToString();
            dynamic response = await sg.client.templates._(template_id).versions._(version_id).patch(requestBody: data);
            LogResponseSG(response);

            return response;
        }


        #endregion


        #region Activate Template

        ///**************************************************************
        /// <summary>
        ///     Activate a transactional template version.
        ///     POST /templates/{template_id}/versions/{version_id}/activate
        /// </summary>
        /// <param name="template_id"> Template ID </param>
        /// <param name="version_id"> Version ID </param>
        /// <returns></returns>
        ///**************************************************************
        public static async Task<IAsyncResult> ActivateTemplate(string template_id, string version_id)
        {
            dynamic response = await sg.client.templates._(template_id).versions._(version_id).activate.post();
            LogResponseSG(response);

            return response;
        }


        #endregion


        #region Log

        ///**************************************************************
        /// <summary>
        ///     Logs response from SendGrid
        /// </summary>
        /// <param name="response">
        ///     Dynamic response in <see cref="SendGridAPIClient"/>
        /// </param>
        ///**************************************************************
        public static void LogResponseSG(dynamic response)
        {
            Console.WriteLine(response.StatusCode);
            Console.WriteLine(response.Body.ReadAsStringAsync().Result);
            Console.WriteLine(response.Headers.ToString());
            Console.ReadLine();
        }

        #endregion
    }
}
