****This project has been discontinued. 

# O-Scav MVP

[O-Scav] is a tech startup with the desire to make scavenger hunts easy to use and make and a new and exciting platform for Destination Marketing Organizations. 

This is currently our first iteration, a web application that will allow businesses and destination marketers to create scavenger hunts using the latest cloud technologies and social media platforms. Participants of the hunt will be using a mobile client which will track their location as they solve clues.


## Program Architecture Summary

The web app is made using ASP.NET MVC 5 with RazorViews and EntityFramework 6 for modeling the data access layers and database.



### Dependencies

The dependencies listed here are those that are not specific to this project. Examples include libraries for services like Azure or SendGrid, or Caching.

Data
EntityFramework 6

Authentication
- Microsoft.Owin
- Microsoft.AspNet.Identity.*
- Microsoft.Owin.Security.Google
- Microsoft.Owin.Security.OAuth

Cloud
- Microsoft.Azure.

Client-Side
- jQuery
- Bootstrap
- CSS
- HTML

APIs
- SendGrid.CSharp.HTTP.Client
- SendGrid.SmtpApi
- SendGrid.Net40


## Getting Started

To start, fork a copy of dev1 or latest up to date branch.
Set O_ScavProtoType.UI as the startup project.

### Configuration

See Web.config in O_ScavProtoType.UI project.

ConnectionStrings.config - connection to SQL Server, local when debugging


### Setting up Database

For development, use (LocalDB)/MSSQLLocalDB as the 'Data Source' in the ConnectionString.config.

<i>For EF 6</i>

When testing, use a local DB. Change the connectionString values Data Source and Encrypt:

```
Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=oscav;Integrated Security=False;User ID=[username];Password=[password];Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False
```


## Bugs and Issues

Below is a list of pending defects/issues that need resolution, with dates reflecting what current bugs are being worked on.

<i>10-11-2016</i>


- User login/authentication error
- HTTPS configurations
- Login redirection - auth attributes/routing





## Team Members
Jim Castrigano