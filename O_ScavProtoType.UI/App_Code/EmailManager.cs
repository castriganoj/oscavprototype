﻿using O_ScavProtoType.UI.App_LocalResources;
using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Web;

///-----------------------------------------------------------------------------------------------
/// <summary>
///     Summary not available.
/// </summary>
///-----------------------------------------------------------------------------------------------
namespace O_ScavProtoType.UI.App_Code
{
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    /// Contains static methods for returning explore@o-scav.com email network credentials
    /// </summary>
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public class EmailManager
    {
        public string MailGmail { get { return ConfigurationManager.AppSettings["mailGoogle"]; } }
        public string MailPassword { get { return ConfigurationManager.AppSettings["gmailPassword"]; } }
        public string MailHost { get { return ConfigurationManager.AppSettings["mailSmtpHost"]; } }

        public int MailPort { get { return Services.Common.Constants.O_ScavGmailPort; } }
        public bool MailEnableSsl { get { return bool.Parse(ConfigurationManager.AppSettings["enableSSL"]); } }
        public int MailTimeout { get { return int.Parse(ConfigurationManager.AppSettings["mailTimeout"]); } }

        static MailMessage message;
        static string mailResultText;
        static bool mailSent = false;

        private HttpRequestBase Request { get; set; }
        public string MailDomain{ get; set; }

        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        ///     Summary not available.
        /// </summary>
        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public EmailManager()
        {
        }

        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        ///     Summary not available.
        /// </summary>
        /// <param name="request">
        ///     Description not available.
        /// </param>
        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public EmailManager(HttpRequestBase request)
        {
            this.Request = request;

            if (Request == null)
                MailDomain = "";
            else
                MailDomain = (Request.IsLocal) ? "https://localhost" : "www.o-scav.com";
        }

        ///***********************************************
        /// <summary>
        ///     Static class containing strings.
        /// </summary>
        ///***********************************************
        public static class MessageStrings
        {
            public static string AnchorLink
            {
                get { return "<a href=\"{0}\">{1}</a>"; }
            }

            public static string OScavThankYou
            {
                get { return "<div><p>Thanks," + Environment.NewLine + "</br>The O-Scav Team</p></div>"; }
            }

            public static string BetaUserEmailSubject
            {
                get { return "Welcome to O-Scav"; }
            }


            public static string RegistrationMessageSubject
            {
                get { return "O-Scav Registration"; }
            }


            /*
             *
             *
             *
             * <div><p>You are receiving this email because you stated your interest in trying out O-Scav pre-release. As we continue to develop our product, we will send you updates whenever we add new features to our product.Along the way, we will also sometimes send you surveys about the product so that we can make sure we’re on the right track. Your feedback will be greatly appreciated!</p></div>
             *
             */
            public static string BetaUserEmailBody
            {
                get
                {
                    return Resources.EmailMessage_HtmlInterest + OScavThankYou;
                }
            }


            public static string RegistrationMessageBody
            {
                get { return "<div><p>Thank you for signing up to use O-Scav! Your registration is almost complete. Please verify your email by clicking "; }

            }

            public static string MessageLink(string link, string linkText = "here")
            {
                return string.Format(AnchorLink, link, linkText) + "</p><p>" + AltClickLinkBody(link);
            }

            public static string AltClickLinkBody(string link)
            {
                return "<p>Or copy and paste following link in browser: " + string.Format(AnchorLink, link, link) + "</p></div>";
            }

            public static string LoginUsername(string username)
            {
                return string.Format("<div><p>Your login is: {0} </p></div>", username);
            }


        }


        public NetworkCredential GetEmailNetworkCredentials()
        {

            var credential = new NetworkCredential()
            {
                UserName = MailGmail,
                Password = MailPassword
            };
            credential.Domain = MailDomain;

            return credential;
        }


        public SmtpClient GetSmtpClientToEmail( )
        {
            var client = new SmtpClient()
            {
                Host = MailHost,
                Port = 465,
                EnableSsl = MailEnableSsl,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };


            return client;
        }





        public MailMessage GetMailMessageToBeta(string toEmail)
        {
            string fromEmail = MailGmail;

            var message = new MailMessage(fromEmail, toEmail);
            message.IsBodyHtml = true;
            message.Subject = MessageStrings.BetaUserEmailSubject;
            message.Body = MessageStrings.BetaUserEmailBody;
            message.Sender = new MailAddress(fromEmail);


            return message;
        }


        public MailMessage RegistrationMessage(string toEmail, string name, string registerLink)
        {
            string fromEmail = MailGmail;

            var message = new MailMessage(fromEmail, toEmail);

            message.Subject = MessageStrings.RegistrationMessageSubject;

            message.Body = MessageStrings.RegistrationMessageBody;
            message.Body += MessageStrings.RegistrationMessageBody + MessageStrings.MessageLink(registerLink);
            message.Body += MessageStrings.LoginUsername(toEmail) + MessageStrings.OScavThankYou;


            return message;
        }


    }
}