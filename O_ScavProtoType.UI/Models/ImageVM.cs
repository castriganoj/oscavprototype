﻿using O_ScavProtoType.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace O_ScavProtoType.UI.Models
{
    public class ImageVM
    {
        public int Id { get; set; }
        public string TaskText { get; set; }
        public ImageTask Image { get; set; }
        public int poiID { get; set; }
    }
}