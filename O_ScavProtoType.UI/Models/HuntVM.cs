﻿using O_ScavProtoType.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace O_ScavProtoType.UI.Models
{
    public class HuntVM
    {
        public Hunt Hunt { get; set; }

        public List<POI> pois{ get; set; }

        public POI NewPoi{ get; set; }


        public HuntVM()
        {
            pois = new List<POI>();
        }
    }
}