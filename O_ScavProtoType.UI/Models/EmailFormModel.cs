using O_ScavProtoType.Services.Common;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace O_ScavProtoType.UI.Models
{

    public class EmailFormModel
    {
        [Required, Display(Name = "Your name")]
        public string FromName { get; set; }
        [Required, Display(Name = "Your email"), EmailAddress]
        public string FromEmail { get; set; }
        [Required]
        public string Message { get; set; }

        public string ToEmail
        {
            get { return "explore@o-scav.com"; }
        }

        //TODO: obtain from Web.config
        public string Password
        {
            get { return Constants.O_ScavGmailPwd; }
        }


        public HttpPostedFileBase Upload { get; set; }
    }
}