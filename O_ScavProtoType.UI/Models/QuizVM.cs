﻿using O_ScavProtoType.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace O_ScavProtoType.UI.Models
{
    public class QuizVM
    {
        public int Id { get; set; }
        public string TaskText { get; set; }
        public QuizAnswers QuizAnswers { get; set; }
        public string CorrectAnswer { get; set; }
        public int poiID { get; set; }
    }
}