﻿using O_ScavProtoType.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace O_ScavProtoType.UI.Models
{
    public class PoiVM
    {

        public POI Poi { get; set; }

        public List<Task> Task { get; set; }

        public Task NewTask { get; set; }

        public TaskTypeNum taskType { get; set; }


        public PoiVM()
        {
            this.NewTask = new Task();
        }
    }
}