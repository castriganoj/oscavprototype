﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace O_ScavProtoType.UI.Models
{
    public class Confirmation<AppUser>
    {
        public string Name { get; set; }

        public string status { get; set; }

        public AppUser Data { get; set; }
    }
}