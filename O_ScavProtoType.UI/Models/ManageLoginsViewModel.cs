﻿using O_ScavProtoType.Models;
using System.ComponentModel.DataAnnotations;

namespace O_ScavProtoType.UI
{
    public class ManageLoginsViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage =
            "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage =
            "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }


    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    ///     Controls user role selection (can access if Administrator or Builder).
    ///     Allows selection of multiple users.
    /// </summary>
    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //public class SelectUserRolesViewModel
    //{
    //    public SelectUserRolesViewModel()
    //    {
    //        this.Roles = new List<SelectRoleEditorViewModel>();
    //    }


    //    // Enable initialization with an instance of ApplicationUser:
    //    public SelectUserRolesViewModel(Auth.IdentityModels.AppUser user)
    //    {
    //        this.UserName = user.UserName;
    //        this.FirstName = user.AppUserInfo.FirstName;
    //        this.LastName = user.AppUserInfo.LastName;

    //        var Db = new UserDbContext();

    //        // Add all available roles to the list of EditorViewModels:
    //        var allRoles = Db.Roles;
    //        foreach (var role in allRoles)
    //        {
    //            // An EditorViewModel will be used by Editor Template:
    //            var rvm = new SelectRoleEditorViewModel();
    //            this.Roles.Add(rvm);
    //        }

    //        // Set the Selected property to true for those roles for
    //        // which the current user is a member:
    //        foreach (var userRole in user.Roles)
    //        {
    //            var checkUserRole =
    //                this.Roles.Find(r => r.RoleName == user.Roles.Any(x => x.RoldId );
    //            checkUserRole.Selected = true;
    //        }
    //    }

    //    public string UserName { get; set; }
    //    public string FirstName { get; set; }
    //    public string LastName { get; set; }
    //    public List<SelectRoleEditorViewModel> Roles { get; set; }
    //}

    public class EditUserViewModel
    {
        public EditUserViewModel() { }

        // Allow Initialization with an instance of ApplicationUser:
        public EditUserViewModel(ApplicationUser user)
        {
            this.UserName = user.UserName;
            this.Email = user.Email;
            this.FirstName = user.AppUserInfo.FirstName;
            this.LastName = user.AppUserInfo.LastName;
        }

        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Organization")]
        public string Organization { get; set; }
    }


    /// <summary>
    ///     Summary: Used to display a single role with a checkbox, within a list structure:
    /// </summary>
    public class SelectRoleEditorViewModel
    {
        public SelectRoleEditorViewModel() { }

        // Accepts an argument of type AppRole
        public SelectRoleEditorViewModel(AppRole role)
        {
            this.RoleName = role.Name;

            // Assign the new Description property:
            this.Description = role.Description;
        }

        public bool Selected { get; set; }

        [Required]
        public string RoleName { get; set; }

        // Add the new Description property:
        public string Description { get; set; }
    }


    public class RoleViewModel
    {
        public string RoleName { get; set; }
        public string Description { get; set; }

        public RoleViewModel() { }
        public RoleViewModel(AppRole role)
        {
            this.RoleName = role.Name;
            this.Description = role.Description;
        }
    }

}