﻿using System.Web;
using System.Web.Mvc;

namespace O_ScavProtoType.UI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            // Prevents anonymous users from accessing methods without role attribute
            // AllowAnonymous attribute overrides the filter
            filters.Add(new AuthorizeAttribute());


            // Requires all access to web app to go through HTTPS
            filters.Add(new RequireHttpsAttribute());
        }
    }
}
