﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI.WebControls;

namespace O_ScavProtoType.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "home",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "home", action = "index", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "hunts", action = "index", id = UrlParameter.Optional }
                );

            //Hunts/4/poi/1

            routes.MapRoute(
                name: "OneLevelNested",
                url: "{controller}/{huntID}/{action}/{poiID}",
                defaults: new { controller = "hunts", action = "index", id = UrlParameter.Optional }
                );



        }
    }
}
