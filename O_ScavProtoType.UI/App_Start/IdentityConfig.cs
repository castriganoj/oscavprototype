﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
//using O_ScavProtoType.DAL;
using System.Data.Entity;
using System.Web;


namespace O_ScavProtoType.UI
{


    #region Moved to O_ScavProtoType.Auth
    //#region Email Services

    /////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ///// <summary>
    /////    Email service uses SendGrid API
    ///// </summary>
    ///// <remarks>
    /////     Method based on blog post <see cref=""/>
    ///// </remarks>
    /////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //public class EmailService : IIdentityMessageService
    //{
    //    #region Fields

    //    private readonly string OScavEmailPwd = Services.Common.Constants.O_ScavMailPwd;

    //    private readonly string OScavClient = Services.Common.Constants.O_ScavGmailSmtp;

    //    private readonly int OScavGmailPort = Services.Common.Constants.O_ScavGmailPort;

    //    private readonly bool IsOScavSSLEnabled = Services.Common.Constants.O_ScavGmailSsl;

    //    private readonly string OScavEmailAddress = Services.Common.Constants.SendGrid_EmailAddress;

    //    private readonly string OScavSendGridUsername = Services.Common.Constants.SendGrid_Username;

    //    private readonly string OScavSendGridPwd = Services.Common.Constants.SendGrid_Pwd;

    //    private readonly string OScavSendGridDisplay = Services.Common.Constants.SendGrid_DisplayName;

    //    #endregion

    //    #region Properties

    //    ////////////////////////////////////////
    //    /// <summary>
    //    ///     Set this before sending email
    //    /// </summary>
    //    ////////////////////////////////////////
    //    public string EmailFrom { get; set; }

    //    ////////////////////////////////////////
    //    /// <summary>
    //    ///     This should be new users email.
    //    /// </summary>
    //    ////////////////////////////////////////
    //    public string EmailTo { get; set; }

    //    ////////////////////////////////////////
    //    /// <summary>
    //    ///     This is the user's full name
    //    /// </summary>
    //    ////////////////////////////////////////
    //    public string DisplayName { get; set; }

    //    #endregion

    //    #region Constructors

    //    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //    /// <summary>
    //    ///     Email service empty constructor
    //    /// </summary>
    //    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //    public EmailService() : base() { }

    //    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //    /// <summary>
    //    ///     Constructor for email service
    //    /// </summary>
    //    /// <param name="emailFrom">
    //    ///     This is O-Scav email
    //    /// </param>
    //    /// <param name="emailTo">
    //    ///     Users email
    //    /// </param>
    //    /// <param name="displayName">
    //    ///     Display name
    //    /// </param>
    //    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //    public EmailService(string emailFrom, string emailTo, string displayName)
    //    {
    //        this.EmailTo = emailTo;
    //        this.EmailFrom = emailFrom;
    //        this.DisplayName = displayName;
    //    }

    //    #endregion


    //    #region Methods

    //    ///*****************************************************************
    //    /// <summary>
    //    ///     Sends a message asyncronously - main message method.
    //    /// </summary>
    //    /// <param name="message">
    //    ///     Message to be sent.
    //    /// </param>
    //    /// <returns>
    //    ///     Threading Task or null.
    //    /// </returns>
    //    ///*****************************************************************
    //    public Task SendAsync(IdentityMessage message)
    //    {
    //        var credentialUserName = Services.Common.Constants.O_ScavGmail;
    //        var sentFrom = credentialUserName;

    //        try
    //        {
    //            return SendMail_Route1(message, credentialUserName, sentFrom);
    //        }
    //        catch (Exception ex)
    //        {
    //            Services.Common.WriteLog.LogWriteConsole(ex);
    //            return null;
    //        }
    //        finally
    //        {
    //            configSendGridasync(message);
    //        }
    //    }

    //    ///******************************************************
    //    /// <summary>
    //    ///     Sends mail using SendGrid C# API with mail v3.
    //    /// </summary>
    //    /// <returns></returns>
    //    ///******************************************************
    //    public static async Task SendMail_SendGrid_Async()
    //    {
    //        string apiKey = Environment.GetEnvironmentVariable("SENDGRID_API_KEY",
    //                            EnvironmentVariableTarget.User);
    //        dynamic sg = new SendGridAPIClient(apiKey);

    //        Email from = new Email("test@example.com");
    //        string subject = "Sending with SendGrid is Fun";
    //        Email to = new Email("test@example.com");
    //        Content content = new Content("text/plain", "and easy to do anywhere, even with C#");
    //        Mail mail = new Mail(from, subject, to, content);

    //        dynamic response = await sg.client.mail.send.post(requestBody: mail.Get());
    //    }


    //    ///******************************************************
    //    /// <summary>
    //    ///     Sends mail using ASP.NET standard route.
    //    /// </summary>
    //    /// <param name="message">
    //    ///     IdentityMessage for user.
    //    /// </param>
    //    /// <param name="credentialUserName">
    //    ///     O-Scav credentials
    //    /// </param>
    //    /// <param name="sentFrom">
    //    ///     O-Scav gmail
    //    /// </param>
    //    /// <returns>
    //    ///
    //    /// </returns>
    //    ///******************************************************
    //    public Task SendMail_Route1(IdentityMessage message, string credentialUserName, string sentFrom)
    //    {


    //        // Configure the client (from System.Net.Mail)
    //        var client = new SmtpClient(OScavClient);

    //        client.Port = OScavGmailPort;
    //        client.EnableSsl = IsOScavSSLEnabled;
    //        client.DeliveryMethod = SmtpDeliveryMethod.Network;
    //        client.UseDefaultCredentials = false;

    //        // Create the O-Scav email credentials:
    //        NetworkCredential credentials = new NetworkCredential(credentialUserName, OScavSendGridPwd);
    //        client.Credentials = credentials;

    //        // Create the message:
    //        var mail = new MailMessage(sentFrom, message.Destination);

    //        mail.Subject = message.Subject;
    //        mail.Body = message.Body;

    //        // Send email for confirming registration
    //        return client.SendMailAsync(mail);
    //    }


    //    ///******************************************************
    //    /// <summary>
    //    ///     Sends a message to user that registered.
    //    /// </summary>
    //    /// <param name="message">
    //    ///     IdentityMessage class containing message with registration linkn
    //    /// </param>
    //    /// <returns>
    //    ///     Description not available
    //    /// </returns>
    //    ///******************************************************
    //    protected Task configSendGridasync(IdentityMessage message)
    //    {

    //        var myMessage = new SendGridMessage();

    //        myMessage.AddTo(message.Destination);
    //        myMessage.From = new MailAddress(OScavEmailAddress, OScavSendGridDisplay);
    //        myMessage.Subject = message.Subject;
    //        myMessage.Text = message.Body;
    //        myMessage.Html = message.Body;

    //        var credentials = new NetworkCredential(OScavEmailAddress, OScavEmailPwd);

    //        // Create a Web transport for sending email.
    //        var transportWeb = new Web(credentials);

    //        // Send the email.
    //        if (transportWeb != null)
    //        {
    //            return transportWeb.DeliverAsync(myMessage);
    //        }
    //        else
    //        {
    //            return Task.FromResult(0);
    //        }
    //    }

    //    ///******************************************************
    //    /// <summary>
    //    ///     Sends email to user normal way
    //    /// </summary>
    //    /// <param name="message">
    //    ///     MailMessage object to be sent
    //    /// </param>
    //    ///******************************************************
    //    public void sendMail(MailMessage message)
    //    {
    //        #region formatter
    //        string text = string.Format("Please click on this link to {0}: {1}", message.Subject, message.Body);
    //        string html = "Please confirm your account by clicking this link: <a href=\"" + message.Body + "\">link</a><br/>";

    //        html += HttpUtility.HtmlEncode(@"Or click on the copy the following link on the browser:" + message.Body);
    //        #endregion

    //        MailMessage msg = new MailMessage();
    //        msg.From = new MailAddress(EmailFrom ?? OScavEmailAddress, DisplayName);
    //        msg.To.Add(new MailAddress(EmailTo));
    //        msg.Subject = message.Subject;

    //        // Message views
    //        msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
    //        msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

    //        SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", Convert.ToInt32(587));
    //        NetworkCredential credentials = new NetworkCredential("joe@contoso.com", "XXXXXX");
    //        smtpClient.Credentials = credentials;
    //        smtpClient.EnableSsl = true;
    //        smtpClient.Send(msg);
    //    }

    //    #endregion
    //}

    //#endregion


    //#region SMS Services

    /////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ///// <summary>
    /////     SMS services
    /////     TODO: add Twilio API here
    ///// </summary>
    /////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //public class SmsService : IIdentityMessageService
    //{
    //    public Task SendAsync(IdentityMessage message)
    //    {
    //        // Plug in your SMS service here to send a text message.
    //        return Task.FromResult(0);
    //    }
    //}

    //#endregion


    //#region Identity Management

    /////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ///// <summary>
    /////     Configure the application user manager used in this application.
    /////     UserManager is defined in ASP.NET Identity and is used by the application.
    ///// </summary>
    /////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //public class ApplicationUserManager : UserManager<AppUser, string>
    //{
    //    public ApplicationUserManager(IUserStore<AppUser, string> store)
    //        : base(store)
    //    {
    //    }

    //    public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
    //    {
    //        var manager = new ApplicationUserManager(new AppUserStore(context.Get<UserDbContext>()));


    //        // Configure validation logic for usernames
    //        manager.UserValidator = new UserValidator<AppUser>(manager)
    //        {
    //            AllowOnlyAlphanumericUserNames = false,
    //            RequireUniqueEmail = true
    //        };

    //        // Configure validation logic for passwords
    //        manager.PasswordValidator = new PasswordValidator
    //        {
    //            RequiredLength = 6,
    //            RequireNonLetterOrDigit = true,
    //            RequireDigit = true,
    //            RequireLowercase = true,
    //            RequireUppercase = true,
    //        };

    //        // Configure user lockout defaults
    //        manager.UserLockoutEnabledByDefault = true;
    //        manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
    //        manager.MaxFailedAccessAttemptsBeforeLockout = 5;

    //        // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
    //        // You can write your own provider and plug it in here.
    //        manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<AppUser>
    //        {
    //            MessageFormat = "Your security code is {0}"
    //        });

    //        manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<AppUser>
    //        {
    //            Subject = "Security Code",
    //            BodyFormat = "Your security code is {0}"
    //        });

    //        manager.EmailService = new EmailService();
    //        manager.SmsService = new SmsService();

    //        var dataProtectionProvider = options.DataProtectionProvider;
    //        if (dataProtectionProvider != null)
    //        {
    //            manager.UserTokenProvider =
    //                new DataProtectorTokenProvider<AppUser>(dataProtectionProvider.Create("ASP.NET Identity"));
    //        }

    //        return manager;
    //    }
    //}


    /////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ///// <summary>
    /////     Custom SignInManager for ASP.NET Identity 2.0 Framework
    ///// </summary>
    /////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //public class ApplicationSignInManager : SignInManager<AppUser, string>
    //{
    //    ApplicationUserManager _userManager;

    //    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //    /// <summary>
    //    ///     Signin manager - contains methods for creating/logging in users
    //    /// </summary>
    //    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //    public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
    //        : base(userManager, authenticationManager)
    //    {
    //        this._userManager = userManager;
    //    }

    //    ///***********************************************
    //    /// <summary>
    //    ///     Static method that returns new ApplicationSignInManager
    //    /// </summary>
    //    /// <param name="options"> IdentityFactoryOptions </param>
    //    /// <param name="context"> IOwinContext </param>
    //    /// <returns>
    //    ///     <see cref="O_ScavProtoType.UI.ApplicationSignInManager"/>
    //    /// </returns>
    //    ///***********************************************
    //    public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
    //    {
    //        return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
    //    }

    //    ///***********************************************
    //    /// <summary>
    //    ///     Generates a user asynchronously
    //    /// </summary>
    //    /// <param name="user">
    //    ///     The AppUser model
    //    /// </param>
    //    /// <returns>
    //    ///     Task returns ClaimsIdentity value
    //    /// </returns>
    //    ///***********************************************
    //    public override async Task<ClaimsIdentity> CreateUserIdentityAsync(AppUser user)
    //    {
    //        if (_userManager == null)
    //        {
    //            var _userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
    //        }

    //        var userIdentity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
    //        return userIdentity;
    //        //return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
    //    }

    //}


    /////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ///// <summary>
    /////     Custom RoleManager for ASP.NET Identity 2.0 Framework.
    ///// </summary>
    /////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //public class ApplicationRoleManager : RoleManager<AppRole>
    //{
    //    public ApplicationRoleManager(IRoleStore<AppRole, string> store)
    //        : base(store)
    //    { }

    //    public static ApplicationRoleManager Create(
    //        IdentityFactoryOptions<ApplicationRoleManager> options,
    //            IOwinContext context)
    //    {
    //        return new ApplicationRoleManager(
    //            new AppRoleStore(context.Get<UserDbContext>()));
    //    }
    //}

    //#endregion
    #endregion


}
