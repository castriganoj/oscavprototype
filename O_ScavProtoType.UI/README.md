# O-Scav MVP

[O-Scav](http://www.o-scav.com/) is a tech startup with the desire to make scavenger hunts easy to use and make and a new and exciting platform for advertising local businesses. 

This is currently our first iteration, a web application that will allow businesses and destination marketers to create scavenger hunts using the latest cloud technologies and social media platforms. Participants of the hunt will be using a mobile client which will track their location as they solve clues. 

We eventually plan on implementing an algorithms that will help make recommendations for builders, similar to how google search engine predicts text, but with location and tasks. 


## Entity Framework

Only 1 migration under o_ScavProtoType.DAL\Migrations folder.

To run migrations, open up a CLI in the migrations folder, then 3 lines to run:

1. 


## Program Architecture Summary

The web app is made using ASP.NET MVC 5 with RazorViews and EntityFramework 6 for modeling the data access layers and database. 
See diagrams found in main Google Drive: O-Scav/Development/Diagrams (TODO: post a detailed diagram of the architecture here).

### Dependencies

The dependencies listed here are those that are not specific to this project. Examples include libraries for services like Azure or SendGrid, or Caching.

<i>Data</i>
- EntityFramework 6  

<i>Authentication</i>
- Microsoft.Owin
- Microsoft.AspNet.Identity.*
- Microsoft.Owin.Security.Google
- Microsoft.Owin.Security.OAuth

<i>Cloud</i>
- Microsoft.WindowsAzure.ConfigurationManager
- Microsoft.Azure.*
- WindowsAzure.Storage

<i>Client-Side</i>

- jQuery.[Validation] 
- bootstrap
 
<i>APIs</i>

- Microsoft.Azure
- SendGrid.CSharp.HTTP.Client
- SendGrid.SmtpApi
- SendGrid.Net40

<i>Caching (NOT IN SERVICE)</i>

- CowCache
- MemCached
- CacheManager
- Microsoft.Extensions.Caching.Memory


## Getting Started

To start, fork a copy of dev1 or latest up to date branch.
Set O_ScavProtoType.UI as the startup project.




## Bugs and Issues

Below is a list of pending defects/issues that need resolution, with dates reflecting what current bugs are being worked on.

<i>10-11-2016</i>


- User login/authentication error
- HTTPS configurations
- Login redirection - auth attributes/routing


## Tasks

Various weekly TODOs:

<i>10-10-2016</i>
- Answers
- ASP.NET Core Project Migrations (part 1)
 
 

## Team Members

Jim Castrigano
Allen Jagoda
Gregory Sun