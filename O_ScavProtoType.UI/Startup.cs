﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(O_ScavProtoType.UI.Startup))]
namespace O_ScavProtoType.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
