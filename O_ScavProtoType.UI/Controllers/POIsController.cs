﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using O_ScavProtoType.DAL;
using O_ScavProtoType.Model;
using O_ScavProtoType.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace O_ScavProtoType.UI.Controllers
{
    public class POIsController : Controller
    {
        private HuntContext db = new HuntContext();
        private HuntRepo repo = new HuntRepo();

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Bad need to either just get 1 hunt or go right to the poi
            var hunts = repo.GetAllHunts();

            POI poi = hunts
                .SelectMany(h => h.POIs
                .Where(p => p.Id == id))
                .SingleOrDefault();

            if (poi == null)
            {
                return HttpNotFound();
            }

            return View(poi);
        }

       public ActionResult Answer(int? taskId)
        {
            if (taskId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Task task = repo.GetTask(taskId);

            if (task == null)
            {
                return HttpNotFound();
            }

            if (task.TaskType.TaskTypeNum == TaskTypeNum.Quiz)
            {
                var quizAttempt = GetQuizAttempt(task.TaskType.Quiz);
                var answers = new List<Answer>();
                answers.Add(null);
                task.Answers = answers;

                return View("AnswerQuiz", quizAttempt);
            }

            else if(task.TaskType.TaskTypeNum == TaskTypeNum.Image)
            {
                var imageAttempt = GetImageAttempt(task.TaskType.Image);
                return View("ImageAnswer", imageAttempt);
            }
            else
            {
               //return to page with error
            }

            //return image or quiz view above
            return View(task);
        }

        [HttpPost]
        public ActionResult AnswerQuiz(QuizAttempt quizAttempt)
        {
            Task taskFromDb = repo.GetTask(quizAttempt.Quiz.TaskType.Id);

            
            string userId = User.Identity.GetUserId();

            quizAttempt.Quiz = taskFromDb.TaskType.Quiz;

            repo.CreateQuizAnswer(quizAttempt, userId);

            return RedirectToAction("Details", new { id = taskFromDb.PoiID });

        }

        //Yikes...This needs to be refactored....:/ 
        [HttpPost]
        public ActionResult AnswerImage(ImageAttempt imageAttempt, HttpPostedFileBase upload)
        {

            if (upload != null && upload.ContentLength > 0)
            {
                var file = new File
                {
                    FileName = System.IO.Path.GetFileName(upload.FileName),
                    FileType = FileType.CorrectImageAnswer,
                    ContentType = upload.ContentType
                };
                using (var reader = new System.IO.BinaryReader(upload.InputStream))
                {
                    file.Content = reader.ReadBytes(upload.ContentLength);
                }

                imageAttempt.Image = file;
            }

            string userId = User.Identity.GetUserId();

            repo.CreateImageAttempt(imageAttempt, userId);

            return RedirectToAction("Details", new { id = 1 });
        }


        private QuizAttempt GetQuizAttempt(QuizTask quiz)
        {
            var quizAttempt = new QuizAttempt();
            quizAttempt.Quiz = quiz;
            
            return quizAttempt;
        }

        private ImageAttempt GetImageAttempt(ImageTask image)
        {
            var imageAtttempt = new ImageAttempt();
            imageAtttempt.CorrectImage = image;

            return imageAtttempt;
        }
    }
}
