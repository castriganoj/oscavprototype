﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using O_ScavProtoType.DAL;
using O_ScavProtoType.Model;

namespace O_ScavProtoType.UI.Controllers
{
    public class FileController : Controller
    {
        private HuntContext db = new HuntContext();

        public ActionResult Index(int id)
        {
            var fileToRetrieve = db.Files.Find(id);

            return File(fileToRetrieve.Content, fileToRetrieve.ContentType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
