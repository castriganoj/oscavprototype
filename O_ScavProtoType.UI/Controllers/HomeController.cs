﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Mail;
using O_ScavProtoType.UI.Models;
using System.Threading.Tasks;
using System.Configuration;
using O_ScavProtoType.UI.App_Code;
using System.Diagnostics;
using Microsoft.AspNet.Identity;
using System.Security;

namespace O_ScavProtoType.UI.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        [AllowAnonymous]
         public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult About()
        {
            ViewBag.Message = "About";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Solutions()
        {
            ViewBag.Message = "Solutions";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.Message = "Contact us";

            return View();
        }

        [AllowAnonymous]
        public ActionResult ComingSoon()
        {
            ViewBag.Message = "Coming Soon";
            return View();
        }

        [AllowAnonymous]
        public ActionResult Sent()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ForPlayers()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ForBuilders()
        {
            return View();
        }



        [HttpPost]
        [RequireHttps]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> SignUpForEmails(EmailFormModel model)
        {
            if (ModelState.IsValid)
            {
                EmailManager em = new EmailManager(Request);

                var userName = em.MailGmail;
                var userPass = em.MailPassword;

                var smtp = new SmtpClient()
                {
                    Host = em.MailHost,
                    Port = em.MailPort,
                    EnableSsl = em.MailEnableSsl,
                    Timeout = em.MailTimeout,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = true
                };
                var message = em.GetMailMessageToBeta(model.ToEmail);

                var credential = new NetworkCredential(userName, userPass);
                smtp.Credentials = credential;

                await smtp.SendMailAsync(message);

                return RedirectToAction("Sent");
            }

            return View(model);
        }






    }
}