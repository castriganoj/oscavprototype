﻿using System;
using System.Collections.Generic;

using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using O_ScavProtoType.DAL;
using O_ScavProtoType.Model;
using System.Diagnostics;
using O_ScavProtoType.UI.Models;
using Microsoft.AspNet.Identity;

namespace O_ScavProtoType.UI.Controllers
{
    //[RequireHttps]
    [Authorize(Roles = "Administrator, Builder")]
    public class HuntsController : Controller
    {
        private HuntRepo Repo = new HuntRepo();

        public ActionResult Index()
        {
            string builderId = User.Identity.GetUserId();
            List<Hunt> hunts = Repo.GetUserHunts(builderId);

            return View(hunts);
        }

        [Authorize]
        public ActionResult Manage()
        {
            return View();
        }

        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Hunt hunt = Repo.GetHunt(id);

            if (hunt == null)
            {
                return HttpNotFound();
            }

            return View(hunt);
        }

        [Authorize(Roles = "Administrator, Builder")]
        public ActionResult Create()
        {
            var huntvm = new HuntVM();
            return View(huntvm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HuntVM huntVm)
        {
            var hunt = new Hunt();
            hunt.Name = huntVm.Hunt.Name;
            hunt.POIs = new List<POI>();
            hunt.POIs.Add(huntVm.NewPoi);

            string userId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                Repo.CreateHunt(hunt, userId);
            }

            return View("Index");
        }


        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Hunt hunt = Repo.GetHunt(id);

            if (hunt == null)
            {
                return HttpNotFound();
            }

            var huntVm = new HuntVM();
            huntVm.Hunt = hunt;
            huntVm.pois = hunt.POIs;

            return View(huntVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HuntVM huntVm)
        {
            //hunt name and 1 poi can change
            Hunt huntToUpdate = Repo.GetHunt(huntVm.Hunt.Id);
            huntToUpdate.Name = huntVm.Hunt.Name;

            if(huntToUpdate.POIs == null)
            {
                huntToUpdate.POIs = new List<POI>() { huntVm.NewPoi };
            }
            else
            {
                huntToUpdate.POIs.Add(huntVm.NewPoi);
            }
            
            if (ModelState.IsValid)
            {
                Repo.UpdateHunt(huntToUpdate);

                return RedirectToAction("edit", new { id = huntToUpdate.Id });
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditPOI(int? poiID)
        {
            if (poiID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var hunts = Repo.GetAllHunts();

            POI poi = hunts
                .SelectMany(h => h.POIs
                .Where(p => p.Id == poiID))
                .SingleOrDefault();

            var PoiVM = new PoiVM();

            PoiVM.Poi = poi;
            PoiVM.Task = poi.Task;

            return View("poi", PoiVM);

        }

        [HttpPost]
        public ActionResult EditPOI(PoiVM poiVm, HttpPostedFileBase upload)
        {
            int huntID = poiVm.Poi.HuntID;
            Hunt huntToUpdate = Repo.GetHunt(huntID);
            POI poi = huntToUpdate.POIs
                .Where(p => p.Id == poiVm.Poi.Id)
                .Single();

            if (ModelState.IsValid)
            {
                huntToUpdate.POIs.Remove(poi);

                if (upload != null && upload.ContentLength > 0)
                {
                    var image = new File
                    {
                        FileName = System.IO.Path.GetFileName(upload.FileName),
                        FileType = FileType.PoiImage,
                        ContentType = upload.ContentType
                    };
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        image.Content = reader.ReadBytes(upload.ContentLength);
                    }
                    //create or replace the file
                    poi.Files = new List<File> { image };
                }

                poi.Name = poiVm.Poi.Name;
                poi.Description = poiVm.Poi.Description;

                
                huntToUpdate.POIs.Add(poi);

                Repo.UpdateHunt(huntToUpdate);

                return RedirectToAction("EditPOI", new { poiID = poi.Id });
            }

            ViewBag.Message = "Item Not Edited";
            return RedirectToAction("Index");
        }

        public ActionResult CreateTasks(PoiVM poiVM)
        {
            var task = poiVM.NewTask;
            if (poiVM.taskType == TaskTypeNum.Quiz)
            {
                var quizVM = new QuizVM();
                quizVM.QuizAnswers = new QuizAnswers();
                quizVM.QuizAnswers.AnswerA = "";
                quizVM.QuizAnswers.AnswerB = "";
                quizVM.QuizAnswers.AnswerC = "";
                quizVM.QuizAnswers.AnswerD = "";
                quizVM.TaskText = task.TaskText;
                quizVM.poiID = poiVM.Poi.Id;
                return View("CreateQuiz", quizVM);
            }

            if (poiVM.taskType == TaskTypeNum.Image)
            {
                var imageVM = new ImageVM();
                imageVM.TaskText = task.TaskText;
                var image = new ImageTask();
                image.Files = new List<File>();
                imageVM.Image = image;
                imageVM.poiID = poiVM.Poi.Id;
                return View("CreateImage", imageVM);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public ActionResult CreateQuizTask(QuizVM quizVM)
        {
            //initalize taskType and Task Objects
            var task = new Task();

            if (ModelState.IsValid)
            {
                var quiz = new QuizTask();
                quiz.TaskType = new TaskType();
                quiz.QuizAnswers = quizVM.QuizAnswers;
                quiz.QuizAnswers.setCorrectAnswer(quizVM.CorrectAnswer);

                //add quiz information
                task.TaskType = quiz.TaskType;
                task.TaskType.Quiz = quiz;

                //add taskType to task
                task.PoiID = quizVM.poiID;
                task.TaskText = quizVM.TaskText;
                task.TaskType.Image = null;
                task.TaskType.TaskTypeNum = TaskTypeNum.Quiz;
                Repo.CreateTask(task);

                return RedirectToAction("Index");
            }

            ViewBag.Message = "Task not Created";
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult CreateImageTask(ImageVM imageVM, HttpPostedFileBase upload)
        {
            //initalize taskType and Task Objects
            var task = new Task();
            task.TaskType = new TaskType();

            var image = new ImageTask();

            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    var file = new File
                    {
                        FileName = System.IO.Path.GetFileName(upload.FileName),
                        FileType = FileType.CorrectImageAnswer,
                        ContentType = upload.ContentType
                    };
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        file.Content = reader.ReadBytes(upload.ContentLength);
                    }

                    image.Files = new List<File> { file };
                }

                task.TaskType.TaskTypeNum = TaskTypeNum.Image;

                //add image information
                task.TaskType.Image = image;

                //add taskType to task
                task.PoiID = imageVM.poiID;
                task.TaskText = imageVM.TaskText;
                task.TaskType.Quiz = null;
                Repo.CreateTask(task);

                return RedirectToAction("Index");
            }

            ViewBag.Message = "Task not Created";
            return RedirectToAction("Index");

        }


        public ActionResult EditTask(int id)
        {
            Task task = Repo.GetTask(id);

            if (task.TaskType.TaskTypeNum == TaskTypeNum.Image)
            {
                var imageVM = new ImageVM();
                imageVM.TaskText = task.TaskText;
                imageVM.poiID = task.PoiID;
                imageVM.Image = task.TaskType.Image;
                return View("EditImage", imageVM);
            }
            else
            {
                var quizVM = new QuizVM();
                quizVM.TaskText = task.TaskText;
                quizVM.poiID = task.PoiID;
                quizVM.QuizAnswers = task.TaskType.Quiz.QuizAnswers;
                quizVM.CorrectAnswer = task.TaskType.Quiz.QuizAnswers.CorrectAnsert;
                return View("EditQuiz", quizVM);
            }
        }

        [HttpPost]
        public ActionResult EditImageTask(ImageVM imageVM, HttpPostedFileBase upload)
        {
            Task task = Repo.GetTask(imageVM.Id);

            if (upload != null && upload.ContentLength > 0)
            {
                var file = new File
                {
                    FileName = System.IO.Path.GetFileName(upload.FileName),
                    FileType = FileType.CorrectImageAnswer,
                    ContentType = upload.ContentType
                };
                using (var reader = new System.IO.BinaryReader(upload.InputStream))
                {
                    file.Content = reader.ReadBytes(upload.ContentLength);
                }

                task.TaskType.Image.Files[0] = file;
                task.TaskText = imageVM.TaskText;

                Repo.UpdateImageTask(task);
            };

            return RedirectToAction("Index");

        }

        [HttpPost]
        public ActionResult EditQuizTask(QuizVM quizVM)
        {
            Task task = Repo.GetTask(quizVM.Id);

            task.TaskText = quizVM.TaskText;
            task.TaskType.Quiz.QuizAnswers.setCorrectAnswer(quizVM.CorrectAnswer);
            task.TaskType.Quiz.QuizAnswers.AnswerA = quizVM.QuizAnswers.AnswerA;
            task.TaskType.Quiz.QuizAnswers.AnswerB = quizVM.QuizAnswers.AnswerB;
            task.TaskType.Quiz.QuizAnswers.AnswerC = quizVM.QuizAnswers.AnswerC;
            task.TaskType.Quiz.QuizAnswers.AnswerD = quizVM.QuizAnswers.AnswerD;

            Repo.UpdateQuizTask(task);

            return RedirectToAction("Index");
        }


        public ActionResult DeleteTask(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = Repo.GetTask(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteTaskConfirmed(int id)
        {
            Repo.DeleteTask(id);

            return RedirectToAction("Index");
        }


        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Hunt hunt = Repo.GetHunt(id);

            if (hunt == null)
            {
                return HttpNotFound();
            }

            return View(hunt);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hunt hunt = Repo.GetHunt(id);
            Repo.DeleteHunt(id);
            return RedirectToAction("Index");
        }

    }
}
