﻿using NUnit.Framework;
using O_ScavProtoType.DAL;
using O_ScavProtoType.Model;
using O_ScavProtoType.Models;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.IO;

//At this point I think the order of events for entity framwork
//and my tests are 1) run tests, 2) auto runs migrations in projects
//migration folder, 3) seed database in database set up. 


public class IntegrationBaseClass
{
    private HuntContext Context;
    private string dbCString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=TestOscav;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

    public ApplicationUser TestUserA;
    public ApplicationUser TestUserB;
    public int poiWTaskId;
    public int TaskToEditId;
    public int huntIdForGetHunt;




    [SetUp]
    public void Setup()
    {
        //Need config file to read test db string from
        Context = new HuntContext(dbCString);

        if (!Context.Roles.Any(r => r.Name == "builder"))
        {
            AppRole role = new AppRole();
            role.Id = "1";
            role.Name = "builder";
            role.Description = "builder";
            Context.Roles.Add(role);
            Context.SaveChanges();
        }

        if (!Context.Users.Any(u => u.UserName == "TestUserA"))
        {
            ApplicationUser user = new ApplicationUser();
            user.UserName = "TestUserA";

            AppUserRole UserRole = new AppUserRole();
            UserRole.RoleId = "1";
            var roles = new List<AppUserRole>();

            user.AppUserRoles = roles;

            Context.Users.Add(user);
            Context.SaveChanges();
        }

        if (!Context.Users.Any(u => u.UserName == "TestUserB"))
        {
            ApplicationUser user = new ApplicationUser();
            user.UserName = "TestUserB";

            AppUserRole UserRole = new AppUserRole();
            UserRole.RoleId = "1";
            var roles = new List<AppUserRole>();

            user.AppUserRoles = roles;

            Context.Users.Add(user);
            Context.SaveChanges();
        }


        TestUserA = Context.Users.FirstOrDefault();

        Context.Dispose();

    }


    public List<Hunt> CreateTestHunts()
    {
        List<Hunt> hunts;

        using (var context = new HuntContext(dbCString))
        {
            if (!context.Hunts.Any(h => h.Name == "Cleveland"))
            {
                hunts = CreateHuntObjects();

                context.Hunts.Add(hunts[0]);
            };
        }

        using (var context = new HuntContext(dbCString))
        {
            hunts = context.Hunts.ToList();
        };

        return hunts;
    }

    public List<Hunt> CreateTestHuntsWithMultipleUsers()
    {
        var hunts = new List<Hunt>();

        hunts = CreateTestHunts();
        hunts.AddRange(CreatHuntObjectsForUserB());

        using (var context = new HuntContext(dbCString))
        {

            context.Hunts.AddRange(hunts);
            context.SaveChanges();
        }

        return hunts;
    }

    public Hunt CreateTestHunt()
    {
        Hunt hunt;

        using (var context = new HuntContext(dbCString))
        {

            if (!context.Hunts.Any(h => h.Name == "TestHunt"))
            {
                hunt = new Hunt() { Id = huntIdForGetHunt, Name = "TestHunt" };

                context.Hunts.Add(hunt);
                context.SaveChanges();

            }

            hunt = context.Hunts.First(h => h.Name == "TestHunt");
            huntIdForGetHunt = hunt.Id;
        }

        return hunt;
    }

    public Hunt CreateTestHuntWithPOIs()
    {
        CreateTestHunt();
        string testHuntName =  "TestHunt";
        var hunt = new Hunt();

        using (var context = new HuntContext(dbCString))
        {
            hunt = context.Hunts.Where(h => h.Name == testHuntName).SingleOrDefault();
            var pois = new List<POI>()
            {
                new POI() { Name = "poi before edit A" },
                new POI() {Name  = "poi before edit B" }
            };

            hunt.POIs.AddRange(pois);
            context.SaveChanges();     
        }

        return hunt;
    }


    public Task InsertTaskInDb(Task task)
    {
        using (var context = new HuntContext(dbCString))
        {
            context.Tasks.Add(task);
            context.SaveChanges();
        }
        return task;
    }





    private List<Hunt> CreateHuntObjects()
    {

        return new List<Hunt>()
        {
            new Hunt() {Name = "Chicago", Author = TestUserA},
            new Hunt() {Name = "Cleveland", Author = TestUserA},
            new Hunt() {Name = "Louisville", Author = TestUserA}
        };
    }

    private List<Hunt> CreatHuntObjectsForUserB()
    {
        return new List<Hunt>()
        {
            new Hunt() {Name = "Chicago", Author = TestUserB},
            new Hunt() {Name = "Cleveland", Author = TestUserB},
            new Hunt() {Name = "Louisville", Author = TestUserB}
        };
    }

    

    [TearDown]
    public void TestCleanup()
    {
        using (var context = new HuntContext(dbCString))
        {
            //add check to ensure database name has 'test' in it. 

            //disable constraints
            context.Database.ExecuteSqlCommand("EXEC sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'");

            //delete data from all tables
            context.Database.ExecuteSqlCommand("EXEC sp_MSForEachTable 'DELETE FROM ?'");

            //add constraints back
            context.Database.ExecuteSqlCommand("EXEC sp_msforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all'");

            //reseed identitiy columns
            context.Database.ExecuteSqlCommand("EXEC sp_MSforeachtable 'IF OBJECTPROPERTY(object_id('' ? ''), ''TableHasIdentity'') = 1 DBCC CHECKIDENT ('' ? '', RESEED, 1)'");

        }
    }
}