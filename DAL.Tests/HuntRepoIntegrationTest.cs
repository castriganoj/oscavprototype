﻿using NUnit.Framework;
using O_ScavProtoType.DAL;
using O_ScavProtoType.Model;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System;
using O_ScavProtoType.Models;
using System.IO;

namespace DAL.Tests
{
    //Needed - Manage database connection in config files
    public class HuntRepoIntegrationTest : IntegrationBaseClass
    {

        //only repo should have access to the context. 
        private HuntRepo Repo = new HuntRepo();
        [Test]
        public void CreateHunt_WithHuntNameAndAssociatedUser_AddsHuntRecordToDbAnd_ReturnsItsId()
        {
            Hunt hunt = CreateHuntObjectForUser();

            Repo.CreateHunt(hunt, hunt.Author.Id);

            Assert.IsNotNull(hunt.Id);
        }

        [Test]
        public void GetUserHunts_ReceivesUserid_ReturnsListofThatUsersHunts()
        {
            var huntsToBeRetrieved = CreateTestHunts();

            var retrivedHunts = Repo.GetUserHunts(TestUserA.Id);

            Assert.That(retrivedHunts, Is.EquivalentTo(huntsToBeRetrieved));
            Assert.That(retrivedHunts.All(h => h.Author.Id == TestUserA.Id));
        }

        [Test]
        public void GetHunt_ReceivesHuntId_ReturnsHunt()
        {
            Hunt huntToBeRetrieved = CreateTestHunt();

            Hunt retrievedHunt = Repo.GetHunt(huntIdForGetHunt);

            Assert.That(retrievedHunt.Name, Is.EqualTo(huntToBeRetrieved.Name));
        }

        [Test]
        public void GetAllHunts_ReturnAllHuntsInHuntsTable_()
        {
            int countOfHuntsToBeRetrieved = CreateTestHuntsWithMultipleUsers().Count();

            int countOfRetrivedHunts = Repo.GetAllHunts().Count();

            Assert.That(countOfHuntsToBeRetrieved == countOfRetrivedHunts);
        }

        [Test]
        public void DeleteHunt_ReceivesHuntId_removesHuntObjectFromDb()
        {
            Hunt hunt = CreateTestHunt();

            Repo.DeleteHunt(hunt.Id);

            Hunt huntAfterDelete = Repo.GetHunt(hunt.Id);
            Assert.That(huntAfterDelete, Is.Null);
        }

        [Test]
        public void UpateHunt_ReceviesHuntObjectWithEditedName_UpdatesHuntName()
        {
            Hunt hunt = CreateTestHunt();
            hunt = Repo.GetHunt(hunt.Id);
            string UpdatedHuntName = "NewHuntName";
            hunt.Name = UpdatedHuntName;

            Repo.UpdateHunt(hunt);

            var UpdatedHunt = Repo.GetHunt(hunt.Id);
            Assert.That(UpdatedHunt.Name, Is.EqualTo(UpdatedHuntName));
        }

        //POI's
        [Test]
        public void UpdateHunt_ReceviesHuntObjectWithPois_AddsPOIsToDb()
        {
            Hunt hunt = CreateTestHunt();

            hunt.POIs = new List<POI>()
            {
                new POI() {Name = "Ohio City"},
            };

            var poisBeforePoisAdd = hunt.POIs;


            Repo.UpdateHunt(hunt);

            var UpdatedHuntFromDB = Repo.GetHunt(hunt.Id);

            Assert.AreEqual(poisBeforePoisAdd, UpdatedHuntFromDB.POIs);
            Assert.That(UpdatedHuntFromDB.POIs.Count, Is.GreaterThan(0));

        }

        [Test]
        public void UpdateHunt_ReceviesHuntObjectWithEditedPOIs_UpdatesHuntWithAssociatedPois()
        {
            Hunt hunt = CreateTestHuntWithPOIs();
            string poiNameBeforeEdit = hunt.POIs[0].Name;

            string editedPoiName = "Edited POI";
            hunt.POIs[0].Name = editedPoiName;

            Repo.UpdateHunt(hunt);

            Hunt updatedHunt = Repo.GetHunt(hunt.Id);
            Assert.That(updatedHunt.POIs.Any(poi => poi.Name == editedPoiName));
            Assert.That(updatedHunt.POIs.Any(poi => poi.Name != poiNameBeforeEdit));
        }

        [Test]
        public void UpdateHunt_ReceivesHuntObjectWithPOIsRemoved_DeletesPOIsFromDB()
        {
            Hunt hunt = CreateTestHuntWithPOIs();
            string poiNameBeforeDelete = hunt.POIs[0].Name;
            POI poiToDelete = hunt.POIs.Single(p => p.Name == poiNameBeforeDelete);
            hunt.POIs.Remove(poiToDelete);

            Repo.UpdateHunt(hunt);

            Hunt updatedHunt = Repo.GetHunt(hunt.Id);

            var poiAfterDelete = updatedHunt.POIs.Any(p => p.Name == poiNameBeforeDelete);

            Assert.That(poiAfterDelete , Is.False);

        }


        //Tasks
        [Test]
        public void CreateTask_ReceviesImageTask_InsertsImageTaskInDb()
        {
            //file name v file path problem problem need to add file path to file table. 

            //need a poi id 
            POI poiBeforeTask = CreateTestHuntWithPOIs().POIs.First();
            int poiIdBeforeTask = poiBeforeTask.Id;
            string taskText = "new image task";
            string NewFilePath = @"C:\REPOS\Oscav\o_scavmvpprototype\DAL.Tests\img104.jpg";

            Task task = GetImageTask(poiIdBeforeTask, taskText, NewFilePath);
            string newFileNameBeforeSave = task.TaskType.Image.Files.Single().FileName;

            //excersise create task method. 
            Repo.CreateTask(task);


            //assert that the test hunt has new task in it. 
            Hunt hunt = Repo.GetHunt(poiBeforeTask.HuntID);


            POI poiWUpdatedTask = hunt.POIs.First(p => p.Id == poiIdBeforeTask);
            List<Task> tasks = poiWUpdatedTask.Task;


            bool taskTextBool = tasks.Any(t => t.TaskText == taskText);
            Assert.That(taskTextBool, Is.True);

            Task newTask = tasks.Single(t => t.TaskText == taskText);
            O_ScavProtoType.Model.File newImageFile = newTask.TaskType.Image.Files.Single();

            Assert.That(newImageFile.FileName, Is.EqualTo(newFileNameBeforeSave));

        }

        [Test]
        public void CreateTask_ReceviesQuizTask_InsertsQuizTaskInDb()
        {
            POI poiBeforeTask = CreateTestHuntWithPOIs().POIs.First();
            int poiIdBeforeTask = poiBeforeTask.Id;
            string taskTextBeforeTaskAdded = "new quiz task";
            string quizAnswer = "correctAns";

            Task newTask = GetQuizTask(poiBeforeTask, taskTextBeforeTaskAdded, quizAnswer);

            //excersise create task
            Repo.CreateTask(newTask);


            Hunt hunt = Repo.GetHunt(poiBeforeTask.HuntID);
            POI poiAfterTaskAdded = hunt.POIs.Single(p => p.Id == poiIdBeforeTask);

            //assert poi has a task with text
            Assert.That(poiAfterTaskAdded.Task.Any(t => t.TaskText == taskTextBeforeTaskAdded));

            //assert that task has the quiz answer in it
            Task updatedTask = poiAfterTaskAdded.Task.Single(t => t.TaskText == taskTextBeforeTaskAdded);
            Assert.That(updatedTask.TaskType.Quiz.QuizAnswers.CorrectAnsert == quizAnswer);
        }

        [Test]
        public void UpdateImageTask_ReceivesUpdatedImageTask_UpdatesImageTaskRecord()
        {
            //need a poi id 
            POI poiBeforeTask = CreateTestHuntWithPOIs().POIs.First();
            int poiIdBeforeTask = poiBeforeTask.Id;
            string taskText = "new image task";
            string FileName = @"C:\REPOS\Oscav\o_scavmvpprototype\DAL.Tests\img103.png";

            Task taskToUpdate = GetImageTask(poiBeforeTask, taskText, FileName);

            //send to base to create in db
            Task SavedTask = InsertTaskInDb(taskToUpdate);

            //modify task and send task to db
            string fileName = @"C:\REPOS\Oscav\o_scavmvpprototype\DAL.Tests\img104.jpg";

            var newFile = new O_ScavProtoType.Model.File();
            newFile.FileType = FileType.CorrectImageAnswer;

            FileInfo fileInfo = new FileInfo(fileName);
            newFile.FileName = fileInfo.Name;
            newFile.ContentType = fileInfo.Extension;

            // The byte[] to save the data in
            byte[] data = new byte[fileInfo.Length];

            // Load a filestream and put its content into the byte[]
            using (FileStream fs = fileInfo.OpenRead())
            {
                fs.Read(data, 0, data.Length);
            }

            newFile.Content = data;

            //edit savedTask by replacing old image file with new image file. 
            string updatedTaskText = "updated task text";
            SavedTask.TaskText = updatedTaskText;
            var oldFile = SavedTask.TaskType.Image.Files.Single();
            int fileIndex = SavedTask.TaskType.Image.Files.IndexOf(oldFile);
            SavedTask.TaskType.Image.Files[fileIndex] = newFile;

            Repo.UpdateImageTask(SavedTask);

            Hunt updatedHunt = Repo.GetHunt(poiBeforeTask.HuntID);

            Task updatedTask = updatedHunt.POIs
                                .SelectMany(p => p.Task)
                                .Where(t => t.Id == SavedTask.Id)
                                .Single();

            //assert the updated task has the updated image and image text. 
            Assert.That(updatedTask.TaskText, Is.EqualTo(updatedTaskText));

            //assert file is equal
            Assert.That(updatedTask.TaskType.Image.Files.Single().FileName, Is.EqualTo(newFile.FileName));
        }

        [Test]
        public void UpdateQuizTask_ReceivesUpdatedQuizTask_UpdatesQuizTaskRecord()
        {
            POI poiBeforeTask = CreateTestHuntWithPOIs().POIs.First();
            int poiIdBeforeTask = poiBeforeTask.Id;
            string taskText = "new quiz task";
            string quizAnswer = "correctAns";


            Task quizTask = GetQuizTask(poiBeforeTask, taskText, quizAnswer);

            Task SavedQuizTask = InsertTaskInDb(quizTask);

            //Edit Task correct answer is answer a change to b
            string updatedAnswer = SavedQuizTask.TaskType.Quiz.QuizAnswers.AnswerB;
            SavedQuizTask.TaskType.Quiz.QuizAnswers.setCorrectAnswer(updatedAnswer);


            Repo.UpdateQuizTask(SavedQuizTask);

            Hunt updatedHunt = Repo.GetHunt(poiBeforeTask.HuntID);

            string answerAfterSave = updatedHunt.POIs
                .SelectMany(p => p.Task)
                .Where(t => t.Id == SavedQuizTask.Id)
                .Single().TaskType.Quiz.QuizAnswers.CorrectAnsert.ToString();

            Assert.That(updatedAnswer, Is.EqualTo(answerAfterSave));


        }

        [Test]
        public void DelateTask_ReceivesTaskId_RemovesTaskFromDb()
        {
            POI poiBeforeTask = CreateTestHuntWithPOIs().POIs.First();
            int poiIdBeforeTask = poiBeforeTask.Id;
            string taskText = "new quiz task";
            string quizAnswer = "correctAns";

            Task quizTask = GetQuizTask(poiBeforeTask, taskText, quizAnswer);

            Task SavedQuizTask = InsertTaskInDb(quizTask);

            Repo.DeleteTask(SavedQuizTask.Id);

            Hunt updatedHunt = Repo.GetHunt(poiBeforeTask.HuntID);

            Task TaskAfterSave = updatedHunt.POIs
                .SelectMany(p => p.Task)
                .Where(t => t.Id == SavedQuizTask.Id)
                .FirstOrDefault();

            Assert.That(TaskAfterSave, Is.Null);
        }

        //Answers
        [Test]
        public void CreateQuizAnswer_ReceivesQuizAnswer_InsertsQuizAnswerInDb()
        {
            POI poiBeforeTask = CreateTestHuntWithPOIs().POIs.First();
            string taskText = "new quiz task";
            string quizAnswer = "correctAns";

            Task quizTask = GetQuizTask(poiBeforeTask, taskText, quizAnswer);
            Task savedQuizTask = InsertTaskInDb(quizTask);

            QuizAttempt quizAttempt = new QuizAttempt()
            {
                Answer = "ans",
                Correct = true,
                Quiz = quizTask.TaskType.Quiz,
                User = TestUserA
            };

            int quizAttemptId = Repo.CreateQuizAnswer(quizAttempt).Id;

            QuizAttempt savedQuizAttempt = Repo.GetQuizAttempt(quizAttemptId);

            Assert.That(quizAttempt.Answer, Is.EqualTo(savedQuizAttempt.Answer));
        }

        [Test]
        public void CreateImageAnswer_ReceivesImageAttempt_InsertsImageAttemptInDb()
        {
            //create an image task
            POI poiBeforeTask = CreateTestHuntWithPOIs().POIs.First();
            string taskText = "new image task";
            string FilePath = @"C:\REPOS\Oscav\o_scavmvpprototype\DAL.Tests\img103.png";

            Task taskToUpdate = GetImageTask(poiBeforeTask, taskText, FilePath);
            O_ScavProtoType.Model.File imageForUserAndAdmin = taskToUpdate.TaskType.Image.Files.Single();
            string FileNameBeforeSave = imageForUserAndAdmin.FileName;

            //send to base to create in db
            Task SavedTask = InsertTaskInDb(taskToUpdate);

            //create image attempt
            var imageAttempt = new ImageAttempt()
            {
                AppUser = TestUserA,
                Image = imageForUserAndAdmin,
                CorrectImage = taskToUpdate.TaskType.Image,
                Correct = true

            };

            int imageAttemptId = Repo.CreateImageAttempt(imageAttempt).Id;

            ImageAttempt savedImageAttempt = Repo.GetImageAttempt(imageAttemptId);

            Assert.That(savedImageAttempt.Image.FileName, Is.EqualTo(FileNameBeforeSave));

        }



        private Hunt CreateHuntObjectForUser()
        {
            var hunt = new Hunt();
            hunt.Name = "ClevelandJohnDoe";
            hunt.Author = TestUserA;

            return hunt;

        }

        private Task GetQuizTask(POI poi, string taskText, string quizAnswer)
        {

            //create a quiz task
            QuizTask quiz = new QuizTask();
            quiz.QuizAnswers = new QuizAnswers()
            {
                AnswerA = quizAnswer,
                AnswerB = "ansB",
                AnswerC = "ansC",
                AnswerD = "andD",
            };
            quiz.QuizAnswers.setCorrectAnswer(quizAnswer);

            //create a taskType 
            TaskType taskType = new TaskType();
            taskType.Quiz = quiz;
            taskType.TaskTypeNum = TaskTypeNum.Quiz;

            //create task
            Task newTask = new Task();
            newTask.PoiID = poi.Id;
            newTask.TaskText = taskText;
            newTask.TaskType = taskType;

            return newTask;
        }

        private Task GetImageTask(POI poi, string taskText, string fileName)
        {
            //create a taskType
            TaskType taskType = new TaskType();
            taskType.TaskTypeNum = TaskTypeNum.Image;

            //create a task
            Task task = new Task();
            task.TaskText = taskText;
            task.PoiID = poi.Id;


            //create an ImagaeTask and initialize file list. 
            ImageTask imageTask = new ImageTask()
            {
                Files = new List<O_ScavProtoType.Model.File>()
            };

            //create file
            var file = new O_ScavProtoType.Model.File();
            file.FileType = FileType.CorrectImageAnswer;


            FileInfo fileInfo = new FileInfo(fileName);
            file.FileName = fileInfo.Name;
            file.ContentType = fileInfo.Extension;

            // The byte[] to save the data in
            byte[] data = new byte[fileInfo.Length];

            // Load a filestream and put its content into the byte[]
            using (FileStream fs = fileInfo.OpenRead())
            {
                fs.Read(data, 0, data.Length);
            }

            file.Content = data;

            //add file to list of files in image task
            imageTask.Files.Add(file);

            //set image in taskType
            taskType.Image = imageTask;

            //set taskType in task
            task.TaskType = taskType;

            return task;

        }

        private Task GetImageTask(int poiIdBeforeTask, string taskText, string NewFileName)
        {
            //create new task
            var task = new Task();
            task.PoiID = poiIdBeforeTask;
            task.TaskText = taskText;

            //create new file
            var FileToInsert = new O_ScavProtoType.Model.File();

            FileInfo fileInfo = new FileInfo(NewFileName);
            FileToInsert.FileName = fileInfo.Name;
            FileToInsert.ContentType = fileInfo.Extension;
            byte[] data = new byte[fileInfo.Length];
            using (FileStream fs = fileInfo.OpenRead())
            {
                fs.Read(data, 0, data.Length);
            }
            FileToInsert.Content = data;

            //create list of files
            var newFileList = new List<O_ScavProtoType.Model.File>()
            {
                FileToInsert
            };

            //add files to imageTask
            var imageTask = new ImageTask();
            imageTask.Files = newFileList;


            //create new taskType 
            var taskType = new TaskType();
            taskType.TaskTypeNum = TaskTypeNum.Image;
            taskType.Image = imageTask;

            //add task type to task;
            taskType.Image = imageTask;

            task.TaskType = taskType;

            return task;
        }





    }
}
